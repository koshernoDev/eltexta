#include "utils.h"

int initNewTaxiClientRequest(unsigned char* idBuffer, const unsigned clientID,
                             TaxiClientRequest* tcReq, unsigned char* buffer,
                             unsigned* totalBytes) {
  memset(idBuffer, 0, ID_SIZE);
  snprintf((char*)idBuffer, ID_SIZE, "%u", clientID);
  srandom((int)time(NULL));
  int t = (rand() % MAX_TIME) + 1;
  if (constructNewTaxiClientRequest(tcReq, idBuffer, t) ==
      -1)  // TODO randomize time
  {
    fprintf(stderr, "clientProcess constructNewTaxiClientRequest() == -1\n");

    return -1;
  }

  memset(buffer, 0, BUF_SIZE);
  if (serializeClientRequest(buffer, tcReq, totalBytes) == -1) {
    fprintf(stderr, "clientProcess serializeClientRequest() == -1\n");

    return -1;
  }

  return 0;
}

int clientProcess(const char* servIP, const char* clientPort,
                  const unsigned clientID) {
  // * service connecting
  int serviceFD = -1;
  if ((serviceFD = newIP4TCPClientAndConnect(servIP, clientPort)) == -1) {
    fprintf(stderr, "clientProcess serviceFD == -1\n");

    return -1;
  }

  // * initing new request
  unsigned char idBuffer[ID_SIZE];
  unsigned char buffer[BUF_SIZE];
  unsigned totalBytes = 0;
  TaxiClientRequest tcReq;
  initNewTaxiClientRequest(idBuffer, clientID, &tcReq, buffer, &totalBytes);

  printTaxiClientRequest(&tcReq);
  send(serviceFD, buffer, totalBytes, 0);

  // * after sending routine
  destructTaxiClientRequest(&tcReq);
  memset(idBuffer, 0, ID_SIZE);
  memset(buffer, 0, BUF_SIZE);

  // * receiving response
  recv(serviceFD, buffer, BUF_SIZE, 0);
  TaxiServiceResponse tsRes;
  deserializeAndConstructServiceResponse(buffer, &tsRes);
  fprintf(stdout, "client: ");
  printTaxiServiceResponse(&tsRes);

  memset(buffer, 0, BUF_SIZE);

  recv(serviceFD, buffer, BUF_SIZE, 0);
  deserializeAndConstructServiceResponse(buffer, &tsRes);
  fprintf(stdout, "client: ");
  printTaxiServiceResponse(&tsRes);

  close(serviceFD);
  destructTaxiServiceResponse(&tsRes);
  memset(buffer, 0, BUF_SIZE);

  return 0;
  // // * reading response
  // if (tsRes.code == ACCEPTED) {
  //   fprintf(stdout, "205\n");
  //   send(serviceFD, "HELLO))\0", BUF_SIZE, 0);
  // } else {
  //   fprintf(stdout, "?\n");
  //   return -1;
  // }

  // // * after receiving routine
  // close(serviceFD);
  // destructTaxiServiceResponse(&tsRes);
  // memset(buffer, 0, BUF_SIZE);

  // return 0;
}

int driverProcess(const char* servIP, const char* driverPort,
                  const unsigned driverID) {
  // TODO move commented blocks to funcs
  // * service connecting
  int serviceFD = -1;
  if ((serviceFD = newIP4TCPClientAndConnect(servIP, driverPort)) == -1) {
    fprintf(stderr, "driverProcess: serviceFD == -1\n");

    return -1;
  }

  // * initing new request
  unsigned char idBuffer[ID_SIZE];
  memset(idBuffer, 0, ID_SIZE);
  snprintf((char*)idBuffer, ID_SIZE, "%u", driverID);
  TaxiDriverRequest tdReq;
  if (constructNewTaxiDriverRequest(&tdReq, idBuffer) == -1) {
    fprintf(stderr, "driverProcess: constructNewTaxiDriverRequest() == -1\n");

    return -1;
  }

  unsigned char buffer[BUF_SIZE];
  memset(buffer, 0, BUF_SIZE);
  unsigned totalBytes = 0;
  if (serializeDriverRequest(buffer, &tdReq, &totalBytes) == -1) {
    fprintf(stderr, "driverProcess: serializeDriverRequest() == -1\n");

    return -1;
  }

  // * sending request
  printTaxiDriverRequest(&tdReq);
  send(serviceFD, buffer, totalBytes, 0);

  // * after sending routine
  if (destructTaxiDriverRequest(&tdReq) == -1) {
    fprintf(stderr, "driverProcess: destructTaxiDriverRequest() == -1\n");

    return -1;
  }
  memset(idBuffer, 0, ID_SIZE);
  memset(buffer, 0, BUF_SIZE);

  // * receiving response
  recv(serviceFD, buffer, BUF_SIZE, 0);
  TaxiServiceResponse tsRes;
  deserializeAndConstructServiceResponse(buffer, &tsRes);
  fprintf(stdout, "driver: ");
  printTaxiServiceResponse(&tsRes);

  // * after receiving routine
  destructTaxiServiceResponse(&tsRes);
  memset(buffer, 0, BUF_SIZE);
  close(serviceFD);

  return 0;
}

int finishCurrentProcessesChunk(unsigned* num, unsigned* numLowerBound,
                                unsigned* numUpperBound,
                                const unsigned MAX_CHUNK, const char* type) {
  // fprintf(stdout, "parent: num = %u, lowerBound = %u, upperBound = %u, max =
  // %u\n", *num, *numLowerBound, *numUpperBound, MAX_CHUNK);

  // int status = 0;
  // pid_t pid = 0;
  while (*num >
         *numLowerBound)  // * while num more than current chunk lower bound
  {
    // pid = wait(&status);
    // fprintf(stdout, "parent: exited %s child process with pid = %d, status =
    // %s\n", type == NULL ? "null" : type, pid, status == 0 ? "success" :
    // "failure");

    *num = *num - 1;
  }
  *numLowerBound += MAX_CHUNK;
  *numUpperBound += MAX_CHUNK;
  *num = *numLowerBound;

  return 0;
}
