#include "utils.h"

int main(int argc, char** argv) {
  if (argc != 8) {
    fprintf(stdout,
            "\nUsage: %s 1:<server IP> 2:<client port> 3:<driver port> "
            "4;<MAX_CLIENTS_CHUNK> "
            "5:<CLIENT_CHUNKS> 6:<MAX_DRIVERS_CHUNK> 7:<DRIVER_CHUNKS>\n",
            argv[0]);

    exit(EXIT_FAILURE);
  }

  char servIP[16];
  char clientPort[8];
  char driverPort[8];
  snprintf(servIP, 16, "%s", argv[1]);
  snprintf(clientPort, sizeof(clientPort), "%s", argv[2]);
  snprintf(driverPort, sizeof(driverPort), "%s", argv[3]);
  const unsigned MAX_CLIENTS_CHUNK =
      (unsigned)atoi(argv[4]);  // * max of client processes running in one time
  const unsigned MAX_DRIVERS_CHUNK =
      (unsigned)atoi(argv[6]);  // * max of driver processes running in one time
  const unsigned CLIENT_CHUNKS = (unsigned)atoi(argv[5]);
  const unsigned DRIVER_CHUNKS = (unsigned)atoi(argv[7]);
  const unsigned totalClients = MAX_CLIENTS_CHUNK * CLIENT_CHUNKS;
  const unsigned totalDrivers = MAX_DRIVERS_CHUNK * DRIVER_CHUNKS;
  pid_t clientPid = 1;
  pid_t driverPid = 2;

  fprintf(stdout,
          "\n{ ip = \"%s\", clientPort = \"%s\", driverPort = \"%s\" "
          "MAX_CLIENTS_CHUNK = %u, CLIENT_CHUNKS = "
          "%u, MAX_DRIVERS_CHUNK = %u, DRIVER_CHUNKS = %u }\n\n",
          servIP, clientPort, driverPort, MAX_CLIENTS_CHUNK, CLIENT_CHUNKS,
          MAX_DRIVERS_CHUNK, DRIVER_CHUNKS);

  // * current client number in chunk. [clientNumLowerBound,
  // * clientNumUpperbound)
  unsigned clientNum = 0;
  // * current client. [0, totalClients]
  unsigned clients = 0;
  // * current driver number in chunk. [driverNumLowerBound,
  // * driverNumUpperbound)
  unsigned driverNum = 0;
  // * current driver. [0, totalDrivers]
  unsigned drivers = 0;
  // * increments by MAX_CLIENTS_CHUNK when
  // * MAX_CLIENTS_CHUNK processes were exited
  unsigned clientNumLowerBound = 0;
  // * increments by MAX_DRIVERS_CHUNK when
  // * MAX_DRIVERS_CHUNK processes were exited
  unsigned driverNumLowerBound = 0;
  // * increments by MAX_CLIENTS_CHUNK when
  // * MAX_CLIENTS_CHUNK processes were exited
  unsigned clientNumUpperbound = MAX_CLIENTS_CHUNK;
  // * increments by MAX_DRIVERS_CHUNK when
  // * MAX_DRIVERS_CHUNK processes were exited
  unsigned driverNumUpperbound = MAX_DRIVERS_CHUNK;

  fprintf(stdout, "totalClients = %u, totalDrivers = %u\n\n", totalClients,
          totalDrivers);

  for (;;) {
    if (drivers >= totalDrivers) {
      fprintf(stdout, "parent: drivers queue is empty\n");
    }
    if (clients >= totalClients) {
      fprintf(stdout, "parent: clients queue is empty\n");
    }
    if (drivers >= totalDrivers && clients >= totalClients) {
      fprintf(stdout, "Total drivers = %u\n", driverNum);
      fprintf(stdout, "Total clients = %u\n", clientNum);
      fprintf(stdout, "=====================DONE=====================\n");

      break;
    }

    // * Taxi driver forks
    for (driverNum = driverNumLowerBound;
         (driverNum < driverNumUpperbound) && (drivers <= totalDrivers);
         ++driverNum, ++drivers) {
      // sleep(1);

      if ((driverPid = fork()) == 0) {
        fprintf(stdout, "\tchild: driver process #%u ran\n", drivers);

        if (driverProcess(servIP, driverPort, drivers * 5) == -1) {
          fprintf(stderr, "driverProcess() == -1\n");

          exit(EXIT_FAILURE);
        }
        fprintf(stdout, "\tchild: driver process #%u finished\n", drivers);
        exit(EXIT_SUCCESS);
      } else if (driverPid > 0) {
        fprintf(stdout, "parent: created driver child process with pid = %d\n",
                driverPid);
      } else {
        fprintf(stderr, "parent: driver process fork() == -1\n");
        abort();
      }
    }

    // * Taxi client forks
    for (clientNum = clientNumLowerBound;
         (clientNum < clientNumUpperbound) && (clients <= totalClients);
         ++clientNum, ++clients) {
      // sleep(1);

      if ((clientPid = fork()) == -1) {
        fprintf(stderr, "parent: client process fork() == -1\n");

        abort();
      } else if (clientPid == 0) {
        fprintf(stdout, "\tchild: client process #%u ran\n", clients);

        if (clientProcess(servIP, clientPort, clients * 10) == -1) {
          fprintf(stderr, "clientProcess() == -1\n");

          exit(EXIT_FAILURE);
        }
        fprintf(stdout, "\tchild: client process #%u finished\n", clients);
        exit(EXIT_SUCCESS);
      } else {
        fprintf(stdout, "parent: created client child process with pid = %d\n",
                clientPid);
      }
    }

    continue;  // ! delete me
    if (driverNum < totalDrivers) {
      if (finishCurrentProcessesChunk(&driverNum, &driverNumLowerBound,
                                      &driverNumLowerBound, MAX_DRIVERS_CHUNK,
                                      "driver") == -1) {
        fprintf(stderr, "finishCurrentProcessesChunk() == -1 \n");

        return -1;
      }
    }

    if (clientNum < totalClients) {
      if (finishCurrentProcessesChunk(&clientNum, &clientNumLowerBound,
                                      &clientNumUpperbound, MAX_CLIENTS_CHUNK,
                                      "client") == -1) {
        fprintf(stderr, "finishCurrentProcessesChunk() == -1\n");

        return -1;
      }
    }
  }

  return 0;
}
