#ifndef UTILS_H
#define UTILS_H

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>
#include <poll.h>

#include "protocol.h"

#define MAX_TIME 5

int initNewTaxiClientRequest(unsigned char *idBuffer, const unsigned clientID,
                             TaxiClientRequest *tcReq, unsigned char *buffer,
                             unsigned *totalBytes);

// * client's server connection logic.
// * returns 0 on success, -1 on failure.
int clientProcess(const char *servIP, const char *clientPort,
                  const unsigned clientID);

// * driver's server connection logic.
// * returns 0 on success, -1 on failure.
int driverProcess(const char *servIP, const char *driverPort,
                  const unsigned driverID);

// * waits for MAX_CHUNK processes for target process type: client or driver.
// * returns 0 on success, -1 on failure.
int finishCurrentProcessesChunk(unsigned *num, unsigned *numLowerBound,
                                unsigned *numUpperBound,
                                const unsigned MAX_CHUNK, const char *type);

#endif  // !UTILS_H