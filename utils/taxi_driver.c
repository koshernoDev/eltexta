#include "taxi_driver.h"

int constructNewTaxiDriverRequest(TaxiDriverRequest *tdReq, const unsigned char *id)
{
    if (tdReq == NULL)
    {
        fprintf(stderr, "constructNewTaxiDriverRequest tdReq == NULL\n");

        return -1;
    }
    if (id == NULL)
    {
        fprintf(stderr, "constructNewTaxiDriverRequest id == NULL\n");

        return -1;
    }

    unsigned int len = strlen((char *)id);
    tdReq->id = (unsigned char *)malloc(len * sizeof(unsigned char));
    memset(tdReq->id, 0, len * sizeof(unsigned char));
    strcpy((char *)tdReq->id, (char *)id);

    return 0;
}

int destructTaxiDriverRequest(TaxiDriverRequest *tdReq)
{
    if (tdReq == NULL)
    {
        fprintf(stderr, "destructTaxiDriverRequest tdReq == NULL\n");

        return -1;
    }

    free(tdReq->id);
    tdReq->id = NULL;

    return 0;
}

void printTaxiDriverRequest(const TaxiDriverRequest *tdReq)
{
    if (tdReq == NULL)
    {
        fprintf(stderr, "printTaxiDriverRequest tdReq == NULL\n");

        return;
    }
    if (tdReq->id == NULL)
    {
        fprintf(stderr, "printTaxiDriverRequest tdReq->id == NULL\n");

        return;
    }
    
    fprintf(stdout, "tdReq = { id = \"%s\" }\n", (char *)tdReq->id);
}
