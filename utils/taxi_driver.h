#ifndef TAXI_DRIVER_H
#define TAXI_DRIVER_H

/**
* * Taxi driver request utils
**/

#include "taxi_common.h"

typedef struct TaxiDriverRequest_t
{
    unsigned char *id;
} TaxiDriverRequest;

int constructNewTaxiDriverRequest(TaxiDriverRequest *tdreq, const unsigned char *id);
int destructTaxiDriverRequest(TaxiDriverRequest *tdreq);
void printTaxiDriverRequest(const TaxiDriverRequest *tdreq);

#endif // !TAXI_DRIVER_H