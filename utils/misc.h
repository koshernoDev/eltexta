#ifndef MISC_H
#define MISC_H

#include "taxi_common.h"

// * returns new client socket fd for given ip and port on success.
// * returns -1 on failure.
int newIP4TCPClientAndConnect(const char *ip, const char *port);

// * prints hexadecimal values of given uchar set divided by dot.
void printProtocolBuffer(const unsigned char *buffer);

#endif // !MISC_H
