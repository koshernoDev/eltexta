#include "taxi_client.h"

int constructNewTaxiClientRequest(TaxiClientRequest *tcReq, const unsigned char *id, const unsigned int time)
{
    if (tcReq == NULL)
    {
        fprintf(stderr, "constructNewTaxiClientRequest tcReq == NULL\n");

        return -1;
    }
    if (id == NULL)
    {
        fprintf(stderr, "constructNewTaxiClientRequest id == NULL\n");

        return -1;
    }

    tcReq->id = (unsigned char *)malloc(ID_SIZE * sizeof(unsigned char));
    memset(tcReq->id, 0, ID_SIZE * sizeof(unsigned char));
    strcpy((char *)tcReq->id, (char *)id);

    tcReq->time = time;

    return 0;
}

int destructTaxiClientRequest(TaxiClientRequest *tcReq)
{
    if (tcReq == NULL)
    {
        fprintf(stderr, "destructTaxiClientRequest tcReq == NULL\n");

        return -1;
    }

    free(tcReq->id);
    tcReq->id = NULL;

    return 0;
}

void printTaxiClientRequest(const TaxiClientRequest *tcReq)
{
    if (tcReq == NULL)
    {
        fprintf(stderr, "printTaxiClientRequest tcReq == NULL\n");

        return;
    }
    if (tcReq->id == NULL)
    {
        fprintf(stderr, "printTaxiClientRequest tcReq->id == NULL\n");

        return;
    }

    fprintf(stdout, "tcReq = { id = \"%s\", time = %u }\n", (char *)tcReq->id, tcReq->time);
}
