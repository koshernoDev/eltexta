#include "taxi_service.h"

int constructNewTaxiServiceResponse(TaxiServiceResponse *tsRes, const unsigned int code, const unsigned char *message)
{
    if (tsRes == NULL)
    {
        fprintf(stderr, "constructNewTaxiServiceResponse tsRes == NULL\n");

        return -1;
    }
    if (message == NULL)
    {
        fprintf(stderr, "constructNewTaxiServiceResponse message == NULL\n");

        return -1;
    }

    unsigned int len = strlen((char *)message);
    tsRes->message = (unsigned char *)malloc(len * sizeof(unsigned char));
    memset(tsRes->message, 0, len * sizeof(unsigned char));
    strcpy((char *)tsRes->message, (char *)message);

    tsRes->code = code;

    return 0;
}

int destructTaxiServiceResponse(TaxiServiceResponse *tsRes)
{
    if (tsRes == NULL)
    {
        fprintf(stderr, "destructTaxiServiceResponse tsRes == NULL\n");

        return -1;
    }

    free(tsRes->message);
    tsRes->message = NULL;

    return 0;
}

void printTaxiServiceResponse(const TaxiServiceResponse *tsRes)
{
    if (tsRes == NULL)
    {
        fprintf(stderr, "printTaxiServiceResponse tsRes == NULL\n");

        return;
    }
    if (tsRes->message == NULL)
    {
        fprintf(stderr, "printTaxiServiceResponse tsRes->message == NULL\n");

        return;
    }

    fprintf(stdout, "tsRes = { code = %u, message = \"%s\" }\n", tsRes->code, (char *)tsRes->message);
}
