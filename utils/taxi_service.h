#ifndef TAXI_SERVICE_H
#define TAXI_SERVICE_H

/**
* * Taxi service utils
**/

#include "taxi_common.h"

#define UINT_CODE 0xB4
#define CODE_MASK 0x14

#define STRING_MSG 0xC2
#define MSG_MASK 0x12

typedef struct TaxiServiceResponse_t
{
    unsigned int code;
    unsigned char *message;
} TaxiServiceResponse;

int constructNewTaxiServiceResponse(TaxiServiceResponse *tsRes, const unsigned int code, const unsigned char *message);
int destructTaxiServiceResponse(TaxiServiceResponse *tsRes);
void printTaxiServiceResponse(const TaxiServiceResponse *tsRes);

#endif // !TAXI_SERVICE_H