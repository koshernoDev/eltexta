#ifndef TAXI_COMMON_H
#define TAXI_COMMON_H

/**
* * Taxi common utils
**/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <math.h>
#include <limits.h>

#define SEG_BORDER 0xFF
#define H_SEG_MASK 0x80 // * Header segments mask
#define CLIENT 0xCC
#define DRIVER 0xDD

#define ID_SIZE 16U
#define BUF_SIZE 64U

#define STRING_ID 0xB2
#define ID_MASK 0x02

#endif // !TAXI_COMMON_H