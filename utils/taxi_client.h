#ifndef TAXI_CLIENT_H
#define TAXI_CLIENT_H

/**
* * Taxi client utils
**/

#include "taxi_common.h"

#define UINT_TIME   0xA4
#define TIME_MASK   0x04

typedef struct TaxiClientRequest_t
{
    unsigned int time;
    unsigned char *id;
} TaxiClientRequest;

int constructNewTaxiClientRequest(TaxiClientRequest *tcReq, const unsigned char *id, const unsigned int time);
int destructTaxiClientRequest(TaxiClientRequest *tcReq);
void printTaxiClientRequest(const TaxiClientRequest *tcReq);

#endif // !TAXI_CLIENT_H