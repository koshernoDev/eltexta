#ifndef PROTOCOL_H
#define PROTOCOL_H

#include "misc.h"
#include "taxi_client.h"
#include "taxi_common.h"
#include "taxi_driver.h"
#include "taxi_service.h"

#define POLLINOUT (POLLIN | POLLOUT)
#define LISTENERS_MASK POLLIN
#define CLIENTS_MASK POLLINOUT
// #define CLIENTS_MASK (POLLIN)
// #define CLIENTS_MASK 0

#define INTERNAL_SERVER_ERROR 500U

#define UNKNOWN_PROTOCOL 430U
#define UNKNOWN_ENDPOINT 431U

#define DRIVERS_LIMIT_EXCEED 435U
#define NO_AVAILABLE_DRIVERS 436U
#define NO_AVAILABLE_CLIENTS 437U

#define BAD_REQUEST 404U

#define LIMIT_EXCEED 425U

#define DRIVER_PROCESS_ERROR \
  445U  // ? internal server error for release? this is for debug

#define OK 200U
#define ACCEPTED 205U
#define CLIENT_HANDLED 210U
#define DRIVER_HANDLED 211U
#define CLIENT_GOT_DIRVER 220U
#define DRIVER_PROCESS_FINISHED 230U

// * returns -1 on error, 0 on success
// * serializes unsigned int
int serializeUInt(unsigned char *buffer, const unsigned int source);
// * returns -1 on error, 0 on success
// * deserializes buffer to unsigned int
int deserializeUInt(const unsigned char *buffer, unsigned int *target);

// * returns -1 on error, 0 on success
// * serializes unsigned char
int serializeUChar(unsigned char *buffer, const unsigned char source);
// * returns -1 on error, 0 on success
// * deserializes buffer to unsigned int
int deserializeUChar(const unsigned char *buffer, unsigned char *target);

// * returns -1 on error, 0 on success
// * serializes string
int serializeUCharSet(unsigned char *buffer, const unsigned char *source);
// * returns -1 on error, 0 on success
// * deserialized buffer to string
int deserializeUCharSet(const unsigned char *buffer, unsigned char *target);

// * returns -1 on error, 0 on success
// * serializes client request. Counts total bytes to send(), write() funcs N
// arg
int serializeClientRequest(unsigned char *buffer,
                           const TaxiClientRequest *tcReq,
                           unsigned *totalBytes);
// * returns -1 on error, 0 on success
// * deserializes octets to TaxiClientRequest struct
int deserializeClientRequest(const unsigned char *octets,
                             TaxiClientRequest *tcReq);

// * returns -1 on error, 0 on success
// * serializes service response. Counts total bytes to send(), write() funcs N
// arg
int serializeServiceResponse(unsigned char *buffer,
                             const TaxiServiceResponse *tcRes,
                             unsigned *totalBytes);
// * returns -1 on error, 0 on success
// * deserializes octets to TaxiServiceResponse struct
int deserializeServiceResponse(const unsigned char *octets,
                               TaxiServiceResponse *tcRes);

// * returns -1 on error, 0 on success
// * deserializes type of servise request
int deserializeEndPoint(const unsigned char *octets, unsigned *type);

// * returns -1 on error, 0 on success
// * serializes driver request. Counts total bytes to send(), write() funcs N
// arg
int serializeDriverRequest(unsigned char *buffer,
                           const TaxiDriverRequest *tdReq,
                           unsigned *totalBytes);
// * returns -1 on error, 0 on success
// * deserializes octets to TaxiDriverRequest struct
int deserializeDriverRequest(const unsigned char *octets,
                             TaxiDriverRequest *tdReq);

int deserializeAndConstructClientRequest(const unsigned char *buffer,
                                         TaxiClientRequest *tcReq);
int deserializeAndConstructDriverRequest(const unsigned char *buffer,
                                         TaxiDriverRequest *tdReq);
int deserializeAndConstructServiceResponse(const unsigned char *buffer,
                                           TaxiServiceResponse *tsRes);

#endif  // !PROTOCOL_H