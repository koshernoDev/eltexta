#include "misc.h"

int newIP4TCPClientAndConnect(const char *ip, const char *port)
{
    if (ip == NULL)
    {
        fprintf(stderr, "newIP4TCPClientAndConnect ip == NULL\n");

        return -1;
    }
    if (port == NULL)
    {
        fprintf(stderr, "newIP4TCPClientAndConnect port == NULL\n");

        return -1;
    }

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));

    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(port));
    if (inet_pton(AF_INET, ip, &addr.sin_addr) != 1)
    {
        fprintf(stderr, "newIP4TCPClientAndConnect inet_pton() != 1\n");

        return -1;
    }
    // fprintf(stdout, "addr = { family = %d, port = %d, addr = %u }\n", addr.sin_family, addr.sin_port, addr.sin_addr.s_addr);

    int fd = socket(AF_INET, SOCK_STREAM, 0);
    // fprintf(stdout, "fd = %d\n", fd);
    if (fd == -1)
    {
        fprintf(stderr, "newIP4TCPClientAndConnect socket() == -1\n");

        return -1;
    }

    if (connect(fd, (struct sockaddr *)&addr, sizeof(addr)) == -1)
    {
        fprintf(stderr, "newIP4TCPClientAndConnect connect() == -1\n");

        return -1;
    }

    return fd;
}

void printProtocolBuffer(const unsigned char *buffer)
{
    unsigned segBorders = 0;
    int isFirst = 1;
    unsigned c = 32;
    while (segBorders != 3 && c--)
    {
        segBorders += *buffer == SEG_BORDER ? 1 : 0;
        fprintf(stdout, "%s%x", isFirst == 1 ? "" : ".", *buffer++);
        isFirst = 0;
    }
    puts("");
}
