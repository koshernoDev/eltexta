#include "protocol.h"

int serializeUInt(unsigned char *buffer, const unsigned int source)
{
    if (buffer == NULL)
    {
        fprintf(stderr, "serializeUInt buffer == NULL\n");

        return -1;
    }

    // printf("input value = %u\n", value);

    buffer[0] = source >> 24;
    // printf("buffer[0]: %x, value >> 24: %u\n", buffer[0], value >> 24);

    buffer[1] = source >> 16;
    // printf("buffer[1]: %x, value >> 16: %u\n", buffer[1], value >> 16);

    buffer[2] = source >> 8;
    // printf("buffer[2]: %x, value >> 8: %u\n", buffer[2], value >> 8);

    buffer[3] = source;
    // printf("buffer[3]: %x, value >> 0: %u\n", buffer[3], value);

    return 0;
}

int deserializeUInt(const unsigned char *buffer, unsigned int *target)
{
    if (buffer == NULL)
    {
        fprintf(stderr, "deserializeUInt buffer == NULL\n");

        return -1;
    }
    if (target == NULL)
    {
        fprintf(stderr, "deserializeUInt target == NULL\n");

        return -1;
    }

    *target = 0;
    int rate = 8 * sizeof(unsigned int);
    int bit = -1;

    // puts("");
    // printf("buffer[0] = %u\n", buffer[0]);

    // printf("#%d bit = %d\n", 31, bit);

    for (int i = 0; i < sizeof(unsigned int); ++i)
    {
        // printf("buffer[%d] = %u\n", i, buffer[i]);
        for (int j = 7; j >= 0; --j)
        {
            --rate;
            bit = (buffer[i] >> j) & 1;
            // printf("#%d bit = %d\n", j, bit);
            if (bit == 1)
            {
                *target += (unsigned int)pow(2, rate);
                // printf("value = %u\n", *value);
            }
        }
    }

    return 0;
}

int serializeUChar(unsigned char *buffer, const unsigned char source)
{
    if (buffer == NULL)
    {
        fprintf(stderr, "serializeUChar buffer == NULL\n");

        return -1;
    }

    buffer[0] = source;

    return 0;
}

int deserializeUChar(const unsigned char *buffer, unsigned char *target)
{
    if (buffer == NULL)
    {
        fprintf(stderr, "deserializeUChar buffer == NULL\n");

        return -1;
    }
    if (target == NULL)
    {
        fprintf(stderr, "deserializeUChar target == NULL\n");

        return -1;
    }

    *target = 0;
    int rate = 8 * sizeof(unsigned char);
    int bit = -1;

    // printf("buffer[%d] = %u\n", i, buffer[i]);
    for (int j = 7; j >= 0; --j)
    {
        --rate;
        bit = (*buffer >> j) & 1;
        // printf("#%d bit = %d\n", j, bit);
        if (bit == 1)
        {
            *target += (unsigned char)pow(2, rate);
        }
    }

    return 0;
}

int serializeUCharSet(unsigned char *buffer, const unsigned char *source)
{
    if (buffer == NULL)
    {
        fprintf(stderr, "serializeUCharSet buffer == NULL\n");

        return -1;
    }
    if (source == NULL)
    {
        fprintf(stderr, "serializeUCharSet source == NULL\n");

        return -1;
    }

    while (*source != 0)
    {
        int isSerialized = serializeUChar(buffer++, *source++);
        if (isSerialized != 0)
        {
            return -1;
        }
    }

    return 0;
}

int deserializeUCharSet(const unsigned char *buffer, unsigned char *target)
{
    if (buffer == NULL)
    {
        fprintf(stderr, "deserializeUCharSet buffer == NULL\n");

        return -1;
    }
    if (target == NULL)
    {
        fprintf(stderr, "deserializeUCharSet target == NULL\n");

        return -1;
    }

    while (*buffer != 0)
    {
        int isDeserialized = deserializeUChar(buffer++, target++);
        if (isDeserialized != 0)
        {
            fprintf(stderr, "deserializeUCharSet isDeserialized != 0\n");

            return -1;
        }
    }

    return 0;
}

int serializeClientRequest(unsigned char *buffer, const TaxiClientRequest *tcreq, unsigned int *totalBytes)
{
    if (buffer == NULL)
    {
        fprintf(stderr, "serializeClientRequest buffer == NULL\n");

        return -1;
    }
    if (tcreq == NULL)
    {
        fprintf(stderr, "serializeClientRequest tcreq == NULL\n");

        return -1;
    }
    if (totalBytes == NULL)
    {
        fprintf(stderr, "serializeClientRequest totalBytes == NULL\n");

        return -1;
    }

    size_t headerBytes = (3 + 2) * sizeof(unsigned char);

    size_t idBytes = strlen((char *)tcreq->id) * sizeof(unsigned char);
    size_t timeBytes = sizeof(tcreq->time);
    size_t bodyBytes = idBytes + timeBytes + 2;

    *totalBytes = (headerBytes + bodyBytes) * sizeof(unsigned char);

    // printf("\nheader bytes = %lu, body bytes = %lu, total bytes = %u\n", headerBytes, bodyBytes, *totalBytes);

    int isSerialized = serializeUChar(buffer++, SEG_BORDER); // * begin of header
    isSerialized = serializeUChar(buffer++, CLIENT);         // * client identifier
    isSerialized = serializeUChar(buffer++, UINT_TIME);      // * time - uint
    isSerialized = serializeUChar(buffer++, STRING_ID);      // * id - string
    isSerialized = serializeUChar(buffer++, SEG_BORDER);     // * end of header -> begin of body

    isSerialized = serializeUInt(buffer, tcreq->time);
    buffer += timeBytes;
    isSerialized = serializeUCharSet(buffer, tcreq->id);
    buffer += idBytes;

    isSerialized = serializeUChar(buffer++, 0x00);       // * zero terminated body
    isSerialized = serializeUChar(buffer++, SEG_BORDER); // * end of body

    if (isSerialized != 0)
    {
        fprintf(stderr, "serializeClientRequest isSerialized != 0\n");

        return -1;
    }

    return 0;
}

static int countDataSegmentsInPackage(const unsigned char *octets, unsigned *dataSegments)
{
    if (*octets == SEG_BORDER)
    {
        ++octets;
        while (*octets != SEG_BORDER || NULL)
        {
            ++octets;
            ++*dataSegments;
        }

        if (octets == NULL)
        {
            fprintf(stderr, "Wrong package format. Header should ends with 0xFF\n");

            return -1;
        }

        --octets;
        while (*octets != SEG_BORDER)
        {
            --octets;
        }
    }
    else
    {
        fprintf(stderr, "Wrong package format. Header should begins with 0xFF\n");

        return -1;
    }

    return 0;
}

int deserializeClientRequest(const unsigned char *octets, TaxiClientRequest *tcreq)
{
    if (octets == NULL)
    {
        fprintf(stderr, "deserializeClientRequest octets == NULL\n");

        return -1;
    }
    if (tcreq == NULL)
    {
        fprintf(stderr, "deserializeClientRequest tcreq == NULL\n");

        return -1;
    }

    const unsigned char *head = octets;
    unsigned int dataSegments = 0;
    if (countDataSegmentsInPackage(head, &dataSegments) == -1)
    {
        fprintf(stderr, "deserializeClientRequest countDataSegmentsInPackage() == -1\n");

        return -1;
    }

    --dataSegments; // * minus request type segment

#ifdef DEBUG
    printf("\nclient request. data segments = %u\n", dataSegments);
#endif // DEBUG

    unsigned int isHeaderBegin = 1;
    size_t offsetBytes = (3 + dataSegments) * sizeof(unsigned char);

    while (octets != NULL)
    {
        // * if begin of header
        if ((*octets == SEG_BORDER) && (isHeaderBegin == 1))
        {
#ifdef DEBUG
            printf("head begin octet = %x\n", *octets);
#endif // DEBUG

            isHeaderBegin = 0;
            octets++; // * skip first seg border
            octets++; // * skip request type
        }

        // * if in header
        if ((*octets & H_SEG_MASK) == H_SEG_MASK)
        {
#ifdef DEBUG
            printf("\theader segment = %x", *octets);
#endif // DEBUG

            // * if time
            if (*octets == UINT_TIME)
            {
#ifdef DEBUG
                printf(" (time segment)\n");
#endif // DEBUG

                int isDeserializedTimeSeg = deserializeUInt((unsigned char *)head + offsetBytes, &tcreq->time);
                if (isDeserializedTimeSeg != 0)
                {
                    fprintf(stderr, "deserializeClientRequest isDeserializedTimeSeg != 0\n");

                    return -1;
                }

#ifdef DEBUG
                printf("\t\ttime = %u\n", tcreq->time);
#endif // DEBUG

                offsetBytes += sizeof(unsigned int);
            }
            // * if id
            else if (*octets == STRING_ID)
            {
#ifdef DEBUG
                printf(" (id segment)\n");
#endif // DEBUG

                // printf("\tstrlen(tcreq.id) = %lu\n", strlen((char *)tcreq->id));
                // printf("\thead + offsetBytes = \"%s\", tcreq.id = \"%s\"", head + offsetBytes, tcreq->id);

                int isDeserializedIdSeg = deserializeUCharSet((unsigned char *)head + offsetBytes, tcreq->id);
                if (isDeserializedIdSeg != 0)
                {
                    fprintf(stderr, "deserializeClientRequest isDeserializedIdSeg != 0\n");

                    return -1;
                }

#ifdef DEBUG
                printf("\t\tid = \"%s\"\n", (char *)tcreq->id);
#endif // DEBUG

                size_t len = strlen((char *)tcreq->id);
                offsetBytes += len;
            }
            else
            {
                fprintf(stderr, "Illegal data segment\n");
                return -1;
            }
        }
        else
        {
            fprintf(stderr, "Wrong header segment format. Segment should starts with 1: 1bbb.bbbb\n");

            return -1;
        }

        octets++;

        // * if end of header
        if ((*octets == SEG_BORDER) && (isHeaderBegin == 0))
        {
#ifdef DEBUG
            printf("head end octet = %x\n", *octets);
#endif // DEBUG

            break;
        }
    }

    return 0;
}

int serializeServiceResponse(unsigned char *buffer, const TaxiServiceResponse *tcres, unsigned int *totalBytes)
{
    if (tcres == NULL)
    {
        fprintf(stderr, "serializeServiceResponse tcres == NULL\n");

        return -1;
    }
    if (totalBytes == NULL)
    {
        fprintf(stderr, "serializeServiceResponse totalBytes == NULL\n");

        return -1;
    }

    size_t headerBytes = (2 + 2) * sizeof(unsigned char);

    size_t messageBytes = strlen((char *)tcres->message) * sizeof(unsigned char);
    size_t codeBytes = sizeof(tcres->code);
    size_t bodyBytes = messageBytes + codeBytes + 2;

    *totalBytes = (headerBytes + bodyBytes) * sizeof(unsigned char);

    // printf("header bytes = %lu, body bytes = %lu, total bytes = %u\n", headerBytes, bodyBytes, *totalBytes);

    int isSerialized = serializeUChar(buffer++, SEG_BORDER); // * begin of header
    isSerialized = serializeUChar(buffer++, UINT_CODE);      // * code - uint
    isSerialized = serializeUChar(buffer++, STRING_MSG);     // * message - string
    isSerialized = serializeUChar(buffer++, SEG_BORDER);     // * end of header -> begin of body

    isSerialized = serializeUInt(buffer, tcres->code);
    buffer += codeBytes;
    isSerialized = serializeUCharSet(buffer, tcres->message);
    buffer += messageBytes;

    isSerialized = serializeUChar(buffer++, 0x00);       // * zero terminated body
    isSerialized = serializeUChar(buffer++, SEG_BORDER); // * end of body

    if (isSerialized != 0)
    {
        fprintf(stderr, "serializeServiceResponse isSerialized != 0\n");

        return -1;
    }

    return 0;
}

int deserializeServiceResponse(const unsigned char *octets, TaxiServiceResponse *tcres)
{
    if (octets == NULL)
    {
        fprintf(stderr, "deserializeServiceResponse octets == NULL\n");

        return -1;
    }
    if (tcres == NULL)
    {
        fprintf(stderr, "deserializeServiceResponse tcres == NULL\n");

        return -1;
    }

    const unsigned char *head = octets;
    unsigned int dataSegments = 0;
    if (countDataSegmentsInPackage(head, &dataSegments) == -1)
    {
        fprintf(stderr, "deserializeServiceResponse countDataSegmentsInPackage() == -1\n");

        return -1;
    }

#ifdef DEBUG
    printf("\nclient request. data segments = %u\n", dataSegments);
#endif // DEBUG

    unsigned int isHeaderBegin = 1;
    size_t offsetBytes = (2 + dataSegments) * sizeof(unsigned char);

    while (octets != NULL)
    {
        // * if begin of header
        if ((*octets == SEG_BORDER) && (isHeaderBegin == 1))
        {
#ifdef DEBUG
            printf("head begin octet = %x\n", *octets);
#endif // DEBUG

            isHeaderBegin = 0;
            octets++; // * skip first seg border
        }

        // * if in header
        if ((*octets & H_SEG_MASK) == H_SEG_MASK)
        {
#ifdef DEBUG
            printf("\theader segment = %x", *octets);
#endif // DEBUG

            // * if code
            if (*octets == UINT_CODE)
            {
#ifdef DEBUG
                printf(" (code segment)\n");
#endif // DEBUG

                int isDeserializedCodeSeg = deserializeUInt((unsigned char *)head + offsetBytes, &tcres->code);
                if (isDeserializedCodeSeg != 0)
                {
                    fprintf(stderr, "deserializeServiceResponse isDeserializedCodeSeg != 0\n");

                    return -1;
                }

#ifdef DEBUG
                printf("\t\tcode = %u\n", tcres->code);
#endif // DEBUG

                offsetBytes += sizeof(unsigned int);
            }
            // * if message
            else if (*octets == STRING_MSG)
            {
#ifdef DEBUG
                printf(" (message segment)\n");
#endif // DEBUG

                int isDeserializedMsgSeg = deserializeUCharSet((unsigned char *)head + offsetBytes, tcres->message);
                if (isDeserializedMsgSeg != 0)
                {
                    fprintf(stdout, "deserializeServiceResponse isDeserializedMsgSeg != 0\n");

                    return -1;
                }

                // printf("\tstrlen(tcres.message) = %lu\n", strlen((char *)tcres->message));
                // printf("\thead + offsetBytes = \"%s\", tcres.message = \"%s\"\n", head + offsetBytes, tcres->message);

#ifdef DEBUG
                printf("\t\tmessage = \"%s\"\n", tcres->message);
#endif // DEBUG

                ssize_t len = strlen((char *)tcres->message);
                offsetBytes += len;
            }
            else
            {
                fprintf(stderr, "deserializeServiceResponse Illegal data segment\n");

                return -1;
            }
        }
        else
        {
            fprintf(stderr, "deserializeServiceResponse: Wrong header segment format. Segment should starts with 1: 1bbb.bbbb\n");
            return -1;
        }

        octets++;

        // * if end of header
        if ((*octets == SEG_BORDER) && (isHeaderBegin == 0))
        {
#ifdef DEBUG
            printf("head end octet = %x\n", *octets);
#endif // DEBUG

            break;
        }
    }

    return 0;
}

int deserializeEndPoint(const unsigned char *octets, unsigned *type)
{
    if (octets == NULL)
    {
        fprintf(stderr, "deserializeEndPoint octets == NULL\n");

        return -1;
    }

    ++octets; // * 2-nd protocol octet shall always contain request type: client, driver etc.

    *type = *octets;

    return 0;
}

int serializeDriverRequest(unsigned char *buffer, const TaxiDriverRequest *tdReq, unsigned *totalBytes)
{
    if (buffer == NULL)
    {
        fprintf(stderr, "serializeDriverRequest buffer == NULL\n");

        return -1;
    }
    if (tdReq == NULL)
    {
        fprintf(stderr, "serializeDriverRequest tdReq == NULL\n");

        return -1;
    }
    if (totalBytes == NULL)
    {
        fprintf(stderr, "serializeDriverRequest totalBytes == NULL\n");

        return -1;
    }

    size_t headerBytes = (2 + 2) * sizeof(unsigned char);

    size_t idBytes = strlen((char *)tdReq->id) * sizeof(unsigned char);
    size_t bodyBytes = idBytes + 2;

    *totalBytes = (headerBytes + bodyBytes) * sizeof(unsigned char);

    // printf("\nheader bytes = %lu, body bytes = %lu, total bytes = %u\n", headerBytes, bodyBytes, *totalBytes);

    int isSerialized = serializeUChar(buffer++, SEG_BORDER); // * begin of header
    isSerialized = serializeUChar(buffer++, DRIVER);         // * driver identifier
    isSerialized = serializeUChar(buffer++, STRING_ID);      // * id - string
    isSerialized = serializeUChar(buffer++, SEG_BORDER);     // * end of header -> begin of body

    isSerialized = serializeUCharSet(buffer, tdReq->id);
    buffer += idBytes;

    isSerialized = serializeUChar(buffer++, 0x00);       // * zero terminated body
    isSerialized = serializeUChar(buffer++, SEG_BORDER); // * end of body

    if (isSerialized != 0)
    {
        fprintf(stderr, "serializeDriverRequest isSerialized != 0\n");

        return -1;
    }

    return 0;
}

int deserializeDriverRequest(const unsigned char *octets, TaxiDriverRequest *tdReq)
{
    if (octets == NULL)
    {
        fprintf(stderr, "deserializeDriverRequest octets == NULL\n");

        return -1;
    }
    if (tdReq == NULL)
    {
        fprintf(stderr, "deserializeDriverRequest tdReq == NULL\n");

        return -1;
    }

    const unsigned char *head = octets;
    unsigned int dataSegments = 0;
    if (countDataSegmentsInPackage(head, &dataSegments) == -1)
    {
        fprintf(stderr, "deserializeDriverRequest countDataSegmentsInPackage() == -1\n");

        return -1;
    }

    --dataSegments; // * minus request type segment

#ifdef DEBUG
    printf("\nclient request. data segments = %u\n", dataSegments);
#endif // DEBUG

    unsigned int isHeaderBegin = 1;
    size_t offsetBytes = (3 + dataSegments) * sizeof(unsigned char);

    while (octets != NULL)
    {
        // * if begin of header
        if ((*octets == SEG_BORDER) && (isHeaderBegin == 1))
        {
#ifdef DEBUG
            printf("head begin octet = %x\n", *octets);
#endif // DEBUG

            isHeaderBegin = 0;
            octets++; // * skip first seg border
            octets++; // * skip request type
        }

        // * if in header
        if ((*octets & H_SEG_MASK) == H_SEG_MASK)
        {
#ifdef DEBUG
            printf("\theader segment = %x", *octets);
#endif // DEBUG
            // * if id
            if (*octets == STRING_ID)
            {
#ifdef DEBUG
                printf(" (id segment)\n");
#endif // DEBUG

                // printf("\tstrlen(tdReq.id) = %lu\n", strlen((char *)tdReq->id));
                // printf("\thead + offsetBytes = \"%s\", tdReq.id = \"%s\"", head + offsetBytes, tdReq->id);

                int isDeserializedIdSeg = deserializeUCharSet((unsigned char *)head + offsetBytes, tdReq->id);
                if (isDeserializedIdSeg != 0)
                {
                    fprintf(stderr, "deserializeDriverRequest isDeserializedIdSeg != 0\n");

                    return -1;
                }

#ifdef DEBUG
                printf("\t\tid = \"%s\"\n", (char *)tdReq->id);
#endif // DEBUG

                size_t len = strlen((char *)tdReq->id);
                offsetBytes += len;
            }
            else
            {
                fprintf(stderr, "deserializeDriverRequest: Illegal data segment\n");
                return -1;
            }
        }
        else
        {
            fprintf(stderr, "deserializeDriverRequest: Wrong header segment format. Segment should starts with 1: 1bbb.bbbb\n");

            return -1;
        }

        octets++;

        // * if end of header
        if ((*octets == SEG_BORDER) && (isHeaderBegin == 0))
        {
#ifdef DEBUG
            printf(" head end octet = %x\n", *octets);
#endif // DEBUG

            break;
        }
    }

    return 0; 
}

int deserializeAndConstructClientRequest(const unsigned char *buffer, TaxiClientRequest *tcReq)
{
    static unsigned char idBuffer[ID_SIZE];
    if (memset(idBuffer, 0, ID_SIZE) == NULL)
    {
        fprintf(stderr, "deserializeAndConstructClientRequest memset == NULL\n");

        return -1;
    }
    if (constructNewTaxiClientRequest(tcReq, idBuffer, 0) == -1)
    {
        fprintf(stderr, "deserializeAndConstructClientRequest constructNewTaxiClientRequest == -1\n");

        return -1;
    }
    if (deserializeClientRequest(buffer, tcReq) == -1)
    {
        fprintf(stderr, "deserializeAndConstructClientRequest deserializeClientRequest == -1\n");

        return -1;
    }

    return 0;
}

int deserializeAndConstructDriverRequest(const unsigned char *buffer, TaxiDriverRequest *tdReq)
{
    static unsigned char idBuffer[ID_SIZE];
    if (memset(idBuffer, 0, ID_SIZE) == NULL)
    {
        fprintf(stderr, "deserializeAndConstructDriverRequest memset == NULL\n");

        return -1;
    }
    if (constructNewTaxiDriverRequest(tdReq, idBuffer) == -1)
    {
        fprintf(stderr, "deserializeAndConstructDriverRequest constructNewTaxiDriverRequest == -1\n");

        return -1;
    }
    if (deserializeDriverRequest(buffer, tdReq) == -1)
    {
        fprintf(stderr, "deserializeAndConstructDriverRequest deserializeDriverRequest == -1\n");

        return -1;
    }

    return 0;
}

int deserializeAndConstructServiceResponse(const unsigned char *buffer, TaxiServiceResponse *tsRes)
{
    static unsigned char msgBuffer[ID_SIZE];
    if (memset(msgBuffer, 0, ID_SIZE) == NULL)
    {
        fprintf(stderr, "deserializeAndConstructServiceResponse memset == NULL\n");

        return -1;
    }
    if (constructNewTaxiServiceResponse(tsRes, 0, msgBuffer) == -1)
    {
        fprintf(stderr, "deserializeAndConstructServiceResponse constructNewTaxiClientRequest == -1\n");

        return -1;
    }
    if (deserializeServiceResponse(buffer, tsRes) == -1)
    {
        fprintf(stderr, "deserializeAndConstructServiceResponse deserializeClientRequest == -1\n");

        return -1;
    }

    return 0;
}
