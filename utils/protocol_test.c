#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <string.h>
#include <errno.h>

#include "protocol.h"

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define log_error(M, ...) fprintf(stderr, "[ERROR] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define assertf(A, M, ...)           \
    if (!(A))                        \
    {                                \
        log_error(M, ##__VA_ARGS__); \
        assert(A);                   \
    }

// * Unsigned Int Tests BEGIN
void testSerializeUIntForValue(const unsigned int value, const unsigned char *expected)
{
    printf("testSerializeUIntForValue = %u", value);

    unsigned char *serializedUInt = (unsigned char *)malloc(sizeof(unsigned int) * sizeof(unsigned char));
    memset(serializedUInt, 0, sizeof(unsigned int) * sizeof(unsigned char));

    int isSerialized = serializeUInt(serializedUInt, value);

    assertf(0 == isSerialized,
            "[Test serialize UInt for value = %u] expected value = %d actual value = %d",
            value, 0, isSerialized);

    // printf("value = %u; serialized = %u.%u.%u.%u\n", value, serializedUInt[0], serializedUInt[1], serializedUInt[2], serializedUInt[3]);

    for (int i = 0; i < 4; ++i)
    {
        assertf(expected[i] == serializedUInt[i],
                "[Test serialize UInt for value = %u] expected value = %x actual value = %x",
                value, expected[i], serializedUInt[i]);
    }

    free(serializedUInt);
    serializedUInt = NULL;

    printf(" - [OK]\n");
}

void testSerializeUInt(void)
{
    unsigned char octets[4];

    octets[0] = 127;
    octets[1] = 255;
    octets[2] = 255;
    octets[3] = 255;
    testSerializeUIntForValue(INT_MAX, octets);

    octets[0] = 255;
    octets[1] = 255;
    octets[2] = 255;
    octets[3] = 255;
    testSerializeUIntForValue(UINT_MAX, octets);

    octets[0] = 0;
    octets[1] = 0;
    octets[2] = 0;
    octets[3] = 0;
    testSerializeUIntForValue(0, octets);

    octets[0] = 0;
    octets[1] = 0;
    octets[2] = 0;
    octets[3] = 123;
    testSerializeUIntForValue(123, octets);

    octets[0] = 0;
    octets[1] = 1;
    octets[2] = 226;
    octets[3] = 64;
    testSerializeUIntForValue(123456, octets);

    octets[0] = 7;
    octets[1] = 91;
    octets[2] = 205;
    octets[3] = 21;
    testSerializeUIntForValue(123456789, octets);
}

void testDeserializeUIntForOctets(const unsigned char *octets, const unsigned int expected)
{
    printf("testDeserializeUIntForOctets = %x.%x.%x.%x", octets[0], octets[1], octets[2], octets[3]);

    unsigned int deserializedUInt = 0;
    int isDeserialized = deserializeUInt(octets, &deserializedUInt);

    // printf("deserialized = %u\n", deserializedValue);

    assertf(0 == isDeserialized,
            "[Test deserialize UInt for octets] expected value = %d, actual value = %d\n",
            0, isDeserialized);

    assertf(expected == deserializedUInt,
            "[Test deserialize UInt for octets] expected value = %u, actual value = %u\n",
            expected, deserializedUInt);

    printf(" - [OK]\n");
}

void testDeserializeUInt(void)
{
    unsigned char octets[4];

    octets[0] = 127;
    octets[1] = 255;
    octets[2] = 255;
    octets[3] = 255;
    testDeserializeUIntForOctets(octets, INT_MAX);

    octets[0] = 255;
    octets[1] = 255;
    octets[2] = 255;
    octets[3] = 255;
    testDeserializeUIntForOctets(octets, UINT_MAX);

    octets[0] = 0;
    octets[1] = 0;
    octets[2] = 0;
    octets[3] = 0;
    testDeserializeUIntForOctets(octets, 0);

    octets[0] = 0;
    octets[1] = 0;
    octets[2] = 0;
    octets[3] = 123;
    testDeserializeUIntForOctets(octets, 123);

    octets[0] = 0;
    octets[1] = 1;
    octets[2] = 226;
    octets[3] = 64;
    testDeserializeUIntForOctets(octets, 123456);

    octets[0] = 7;
    octets[1] = 91;
    octets[2] = 205;
    octets[3] = 21;
    testDeserializeUIntForOctets(octets, 123456789);
}
// * Unsigned Int Tests END

// * Unsigned Char Tests BEGIN
void testSerializeUCharForValue(const unsigned char value, const unsigned char expected)
{
    printf("Test serialize UChar for value = %s", value < 128 ? (char *)&value : "not ascii");

    unsigned char *serializedUChar = (unsigned char *)malloc(sizeof(unsigned char));
    int isSerialized = serializeUChar(serializedUChar, value);

    assertf(0 == isSerialized,
            "[Test serialize UChar for value = %x] expected value = %d actual value = %d",
            value, 0, isSerialized);

    // printf("value = %u; serialized = %u.%u.%u.%u\n", value, serializedUChar[0], serializedUChar[1], serializedUChar[2], serializedUChar[3]);

    assertf(expected == *serializedUChar,
            "[Test serialize UChar for value = %u] expected value = %x actual value = %x",
            value, expected, *serializedUChar);

    free(serializedUChar);
    serializedUChar = NULL;

    printf(" - [OK]\n");
}

void testSerializeUChar(void)
{
    testSerializeUCharForValue('a', 'a');
    // testSerializeUCharForValue(-1, UCHAR_MAX);
    // testSerializeUCharForValue(-UCHAR_MAX, 1);
    testSerializeUCharForValue(UCHAR_MAX, UCHAR_MAX);
    testSerializeUCharForValue('1', '1');
    testSerializeUCharForValue('-', '-');
}

void testDeserializeUCharForOctet(const unsigned char octet, const unsigned char expected)
{
    printf("tesetDeserializeUCharForOctet = %x", octet);

    unsigned char deserializedUChar = 0;
    unsigned int isDeserialized = deserializeUChar(&octet, &deserializedUChar);

    assertf(0 == isDeserialized,
            "[Test deserialize UChar for octet] expected value = %d, acutal value = %d\n",
            0, isDeserialized);

    assertf(expected == deserializedUChar,
            "[Test deserialize UChar for octet] expected value = %x, acutal value = %x\n",
            expected, deserializedUChar);

    printf(" - [OK]\n");
}

void testDeserializeUChar(void)
{
    testDeserializeUCharForOctet('b', 'b');
    testDeserializeUCharForOctet(UCHAR_MAX, UCHAR_MAX);
    testDeserializeUCharForOctet('2', '2');
    testDeserializeUCharForOctet('.', '.');
}
// * Unsigned Char Tests END

// * String Tests BEGIN
void testSerializeUCharSetForString(const unsigned char *string, const unsigned char *expectedOctets)
{
    size_t len = strlen((char *)string);
    printf("testSerializeUCharSetForString (len = %lu) = \"%s\"", len, string);

    unsigned char *serializedUCharSet = (unsigned char *)malloc(len * sizeof(unsigned char));
    memset(serializedUCharSet, 0, len * sizeof(unsigned char));

    int isSerialized = serializeUCharSet(serializedUCharSet, string);
    len = strlen((char *)serializedUCharSet);

    assertf(0 == isSerialized,
            "[Test serialize UCharSet (String) for value = \"%s\"] expected value = %d actual value = %d",
            string, 0, isSerialized);

    for (int i = 0; i < len; ++i)
    {
        assertf(expectedOctets[i] == serializedUCharSet[i],
                "[Test serialize UCharSet (String) for value = \"%s\"] expected value = %x actual value = %x",
                string, expectedOctets[i], serializedUCharSet[i]);
    }

    free(serializedUCharSet);
    serializedUCharSet = NULL;

    printf(" - [OK]\n");
}

void testSerializeUCharSet(void)
{
    const unsigned int len = 32;

    unsigned char buffer[len];
    memset(&buffer, 0, len);

    unsigned char octets[len];
    memset(&octets, 0, len);

    {
        snprintf((char *)buffer, len, "abc");
        strcpy((char *)octets, (char *)buffer);

        testSerializeUCharSetForString(buffer, octets);

        memset(buffer, 0, len);
        memset(octets, 0, len);
    }

    {
        snprintf((char *)buffer, len, "abcd");
        strcpy((char *)octets, (char *)buffer);

        testSerializeUCharSetForString(buffer, octets);

        memset(buffer, 0, len);
        memset(octets, 0, len);
    }

    {
        snprintf((char *)buffer, len, "abcd - dcba");
        strcpy((char *)octets, (char *)buffer);

        testSerializeUCharSetForString(buffer, octets);

        memset(buffer, 0, len);
        memset(octets, 0, len);
    }

    {
        snprintf((char *)buffer, len, "            ");
        strcpy((char *)octets, (char *)buffer);

        testSerializeUCharSetForString(buffer, octets);

        memset(buffer, 0, len);
        memset(octets, 0, len);
    }

    {
        snprintf((char *)buffer, len, "abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabc");
        strcpy((char *)octets, (char *)buffer);

        testSerializeUCharSetForString(buffer, octets);

        memset(buffer, 0, len);
        memset(octets, 0, len);
    }

    {
        snprintf((char *)buffer, len, "buf 123 @!#");
        strcpy((char *)octets, (char *)buffer);

        testSerializeUCharSetForString(buffer, octets);

        memset(buffer, 0, len);
        memset(octets, 0, len);
    }
}

void testDeserializeUCharSetForOctets(const unsigned char *octets, const unsigned char *expectedString)
{
    ssize_t len = strlen((char *)octets);
    printf("testDeserializeUCharSetForOctets (len = %lu) = ", len);
    int isFirst = 1;
    for (ssize_t i = 0; i < len; ++i)
    {
        printf("%s%x", isFirst == 1 ? "" : ".", octets[i]);
        isFirst = 0;
    }

    unsigned char *deserializedUCharSet = (unsigned char *)malloc(len * sizeof(unsigned char));
    memset(deserializedUCharSet, 0, len * sizeof(unsigned char));
    int isDeserialized = deserializeUCharSet(octets, deserializedUCharSet);

    // printf("\ndeserialized = %s\n", deserializedUCharSet);

    assertf(0 == isDeserialized,
            "[Test deserialize UCharSet for octets] expected value = %d, actual value = %d\n",
            0, isDeserialized);

    assertf(strcmp((char *)expectedString, (char *)deserializedUCharSet) == 0,
            "[Test deserialize UCharSet for octets] expected value = %s, actual value = %s\n",
            expectedString, deserializedUCharSet);

    free(deserializedUCharSet);
    deserializedUCharSet = NULL;

    printf(" - [OK]\n");
}

void testDeserializeUCharSet(void)
{
    const unsigned int len = 32;

    unsigned char buffer[len];
    memset(&buffer, 0, len);

    unsigned char octets[len];
    memset(&octets, 0, len);

    {
        snprintf((char *)octets, len, "qwe");
        strcpy((char *)buffer, (char *)octets);

        testDeserializeUCharSetForOctets(octets, buffer);

        memset(buffer, 0, len);
        memset(octets, 0, len);
    }

    {
        snprintf((char *)octets, len, "qwer");
        strcpy((char *)buffer, (char *)octets);

        testDeserializeUCharSetForOctets(octets, buffer);

        memset(buffer, 0, len);
        memset(octets, 0, len);
    }

    {
        snprintf((char *)octets, len, "qwer . rewq");
        strcpy((char *)buffer, (char *)octets);

        testDeserializeUCharSetForOctets(octets, buffer);

        memset(buffer, 0, len);
        memset(octets, 0, len);
    }

    {
        snprintf((char *)octets, len, "          ");
        strcpy((char *)buffer, (char *)octets);

        testDeserializeUCharSetForOctets(octets, buffer);

        memset(buffer, 0, len);
        memset(octets, 0, len);
    }

    {
        snprintf((char *)octets, len, "qwerqwerqwerqwerqwerqwervqwerqwerqwer");
        strcpy((char *)buffer, (char *)octets);

        testDeserializeUCharSetForOctets(octets, buffer);

        memset(buffer, 0, len);
        memset(octets, 0, len);
    }

    {
        snprintf((char *)octets, len, "qwerty");
        strcpy((char *)buffer, (char *)octets);

        testDeserializeUCharSetForOctets(octets, buffer);

        memset(buffer, 0, len);
        memset(octets, 0, len);
    }
}
// * String Tests END

// * Client Request Tests BEGIN
void testSerializeClientRequestForClient(const TaxiClientRequest *tcreq, const unsigned char *expectedOctets, const unsigned int expectedTotalBytes)
{
    printf("testSerializeClientRequestForClient { id = \"%s\", time = %u } ", tcreq->id, tcreq->time);

    unsigned char serializedClientRequest[expectedTotalBytes];
    memset(serializedClientRequest, 0, expectedTotalBytes);
    unsigned int bytes = 0;
    int isSerialized = serializeClientRequest(serializedClientRequest, tcreq, &bytes);

    // printProtocolBuffer(serializedClientRequest);
    // printf("\nbytes = %u\n", bytes);

    assertf(0 == isSerialized,
            "[Test serialize client request for client] expected value = %d, acutal value = %d\n",
            0, isSerialized);

    // printf("\nserialized = %s\n", (char *)serializedClientRequest);

    printProtocolBuffer(serializedClientRequest);

    assertf(expectedTotalBytes == bytes,
            "[Test serialize client request for client] expected value = %u, acutal value = %u\n",
            expectedTotalBytes, bytes);

    for (int i = 0; i < bytes; ++i)
    {
        assertf(expectedOctets[i] == serializedClientRequest[i],
                "[Test serialize client request for client] expected value = %u, actual value = %u\n",
                expectedOctets[i], serializedClientRequest[i]);
    }

    printf(" - [OK]\n");
}

void testSerializeClientRequest(void)
{
    {
        TaxiClientRequest tcreq;
        constructNewTaxiClientRequest(&tcreq, (unsigned char *)"ff99aa55", 5555);
        unsigned char expectedOctets[] = {
            SEG_BORDER, CLIENT, UINT_TIME, STRING_ID, SEG_BORDER, 0x00, 0x00, 0x15, 0xB3, 'f', 'f', '9', '9', 'a', 'a', '5', '5', 0x00, SEG_BORDER};
        const unsigned int expectedBytes = 3 + 2 + (sizeof(tcreq.time)) + (strlen((char *)tcreq.id) * sizeof(unsigned char)) + 2;

        testSerializeClientRequestForClient(&tcreq, expectedOctets, expectedBytes);

        destructTaxiClientRequest(&tcreq);
        memset(expectedOctets, 0, expectedBytes);
    }

    {
        TaxiClientRequest tcreq;
        constructNewTaxiClientRequest(&tcreq, (unsigned char *)"abc-cba", 1234);
        unsigned char expectedOctets[] = {
            SEG_BORDER, CLIENT, UINT_TIME, STRING_ID, SEG_BORDER, 0x00, 0x00, 0x04, 0xD2, 'a', 'b', 'c', '-', 'c', 'b', 'a', 0x00, SEG_BORDER};
        const unsigned int expectedBytes = 3 + 2 + (sizeof(tcreq.time)) + (strlen((char *)tcreq.id) * sizeof(unsigned char)) + 2;

        testSerializeClientRequestForClient(&tcreq, expectedOctets, expectedBytes);

        destructTaxiClientRequest(&tcreq);
        memset(expectedOctets, 0, expectedBytes);
    }

    {
        TaxiClientRequest tcreq;
        constructNewTaxiClientRequest(&tcreq, (unsigned char *)"20", 10);
        unsigned char expectedOctets[] = {
            SEG_BORDER, CLIENT, UINT_TIME, STRING_ID, SEG_BORDER, 0x00, 0x00, 0x00, 0x0A, '2', '0', 0x00, SEG_BORDER};
        const unsigned int expectedBytes = 3 + 2 + (sizeof(tcreq.time)) + (strlen((char *)tcreq.id) * sizeof(unsigned char)) + 2;

        testSerializeClientRequestForClient(&tcreq, expectedOctets, expectedBytes);

        destructTaxiClientRequest(&tcreq);
        memset(expectedOctets, 0, expectedBytes);
    }
}

void testDeserializeClientRequestForOctets(unsigned char *octets, const TaxiClientRequest *expectedTCReq)
{
    printf("testDeserializeClientRequestForOctets = ");

    printProtocolBuffer(octets);

    TaxiClientRequest tcreq;
    constructNewTaxiClientRequest(&tcreq, (unsigned char *)"", 0);
    int isDeserialized = deserializeClientRequest(octets, &tcreq);

    assertf(0 == isDeserialized,
            "[Test deserialize client request for octets] expected value = %d, acutal value = %d\n",
            0, isDeserialized);

    assertf(strcmp((char *)expectedTCReq->id, (char *)tcreq.id) == 0,
            "[Test deserialize client request for octets] expected value = %s, acutal value = %s\n",
            expectedTCReq->id, tcreq.id);

    assertf(expectedTCReq->time == tcreq.time,
            "[Test deserialize client request for octets] expected value = %u, actual value = %u\n",
            expectedTCReq->time, tcreq.time);

    destructTaxiClientRequest(&tcreq);

    printf(" - [OK]\n");
}

void testDeserializeClientRequest(void)
{
    {
        TaxiClientRequest expectedTCReq;
        constructNewTaxiClientRequest(&expectedTCReq, (const unsigned char *)"bb77dd55", 6666);
        unsigned char octets[] = {
            SEG_BORDER, CLIENT, UINT_TIME, STRING_ID, SEG_BORDER, 0x00, 0x00, 0x1A, 0x0A, 'b', 'b', '7', '7', 'd', 'd', '5', '5', 0x00, SEG_BORDER};

        testDeserializeClientRequestForOctets(octets, &expectedTCReq);

        destructTaxiClientRequest(&expectedTCReq);
        memset(octets, 0, 19);
    }

    {
        TaxiClientRequest expectedTCReq;
        constructNewTaxiClientRequest(&expectedTCReq, (const unsigned char *)"ert-tre", 12345);
        unsigned char octets[] = {
            SEG_BORDER, CLIENT, UINT_TIME, STRING_ID, SEG_BORDER, 0x00, 0x00, 0x30, 0x39, 'e', 'r', 't', '-', 't', 'r', 'e', 0x00, SEG_BORDER};

        testDeserializeClientRequestForOctets(octets, &expectedTCReq);

        destructTaxiClientRequest(&expectedTCReq);
        memset(octets, 0, 18);
    }
}
// * Client Request Tests END

// * Client Response Tests BEGIN
void testSerializeServiceResponseForResponse(const TaxiServiceResponse *tsRes, const unsigned char *expectedOctets, const unsigned int expectedTotalBytes)
{
    printf("testSerializeServiceResponseForResponse { code = %u, msg = \"%s\" } ", tsRes->code, tsRes->message);

    unsigned char serializedServiceResponse[expectedTotalBytes];
    memset(serializedServiceResponse, 0, expectedTotalBytes);
    unsigned int bytes = 0;
    int isSerialized = serializeServiceResponse(serializedServiceResponse, tsRes, &bytes);

    assertf(0 == isSerialized,
            "[Test serialize service respose for octets] expected value = %d, acutal value = %d\n",
            0, isSerialized);

    printProtocolBuffer(serializedServiceResponse);

    assertf(expectedTotalBytes == bytes,
            "[Test serialize service response for octets] expected value = %u, acutal value = %u\n",
            expectedTotalBytes, bytes);

    for (int i = 0; i < bytes; ++i)
    {
        assertf(expectedOctets[i] == serializedServiceResponse[i],
                "[Test serialize service response for octets] expected value = %u, actual value = %u\n",
                expectedOctets[i], serializedServiceResponse[i]);
    }

    printf(" - [OK]\n");
}

void testSerializeServiceResponse(void)
{
    {
        TaxiServiceResponse tsRes;
        constructNewTaxiServiceResponse(&tsRes, 200, (unsigned char *)"OK");
        unsigned char expectedOctets[] = {
            SEG_BORDER, UINT_CODE, STRING_MSG, SEG_BORDER, 0x00, 0x00, 0x00, 0xC8, 'O', 'K', 0x00, SEG_BORDER};
        const unsigned int expectedBytes = 2 + 2 + (strlen((char *)tsRes.message) * sizeof(unsigned char)) + sizeof(tsRes.code) + 2;

        testSerializeServiceResponseForResponse(&tsRes, expectedOctets, expectedBytes);

        destructTaxiServiceResponse(&tsRes);
        memset(expectedOctets, 0, expectedBytes);
    }

    {
        TaxiServiceResponse tsRes;
        constructNewTaxiServiceResponse(&tsRes, 404, (unsigned char *)"Not Found");
        unsigned char expectedOctets[] = {
            SEG_BORDER, UINT_CODE, STRING_MSG, SEG_BORDER, 0x00, 0x00, 0x01, 0x94, 'N', 'o', 't', ' ', 'F', 'o', 'u', 'n', 'd', 0x00, SEG_BORDER};
        const unsigned int expectedBytes = 2 + 2 + (strlen((char *)tsRes.message) * sizeof(unsigned char)) + sizeof(tsRes.code) + 2;

        testSerializeServiceResponseForResponse(&tsRes, expectedOctets, expectedBytes);

        destructTaxiServiceResponse(&tsRes);
        memset(expectedOctets, 0, expectedBytes);
    }
}

void testDeserializeServiceResponseForOctets(unsigned char *octets, const TaxiServiceResponse *expectedTSRes)
{
    printf("testDeserializeServiceResponseForOctets = ");
    printProtocolBuffer(octets);

    TaxiServiceResponse tsRes;
    constructNewTaxiServiceResponse(&tsRes, -1, (unsigned char *)"");
    int isDeserialized = deserializeServiceResponse(octets, &tsRes);

    assertf(0 == isDeserialized,
            "[Test deserialize client response for octets] expected value = %d, acutal value = %d\n",
            0, isDeserialized);

    assertf(strcmp((char *)expectedTSRes->message, (char *)tsRes.message) == 0,
            "[Test deserialize client response for octets] expected value = %s, acutal value = %s\n",
            expectedTSRes->message, tsRes.message);

    assertf(expectedTSRes->code == tsRes.code,
            "[Test deserialize client response for octets] expected value = %u, actual value = %u\n",
            expectedTSRes->code, tsRes.code);

    destructTaxiServiceResponse(&tsRes);

    printf(" - [OK]\n");
}

void testDeserializeServiceResponse(void)
{
    {
        unsigned char octets[] = {
            SEG_BORDER, UINT_CODE, STRING_MSG, SEG_BORDER, 0x00, 0x00, 0x00, 0xCA, 'A', 'c', 'c', 'e', 'p', 't', 'e', 'd', 0x00, SEG_BORDER};
        TaxiServiceResponse expectedTSRes;
        constructNewTaxiServiceResponse(&expectedTSRes, 202, (unsigned char *)"Accepted");

        testDeserializeServiceResponseForOctets(octets, &expectedTSRes);

        destructTaxiServiceResponse(&expectedTSRes);
        memset(octets, 0, 18);
    }

    {
        unsigned char octets[] = {
            SEG_BORDER, UINT_CODE, STRING_MSG, SEG_BORDER, 0x00, 0x00, 0x01, 0x90, 'B', 'a', 'd', ' ', 'R', 'e', 'q', 'u', 'e', 's', 't', 0x00, SEG_BORDER};
        TaxiServiceResponse expectedTSRes;
        constructNewTaxiServiceResponse(&expectedTSRes, 400, (unsigned char *)"Bad Request");

        testDeserializeServiceResponseForOctets(octets, &expectedTSRes);

        destructTaxiServiceResponse(&expectedTSRes);
        memset(octets, 0, 21);
    }
}
// * Client Response Tests END

void protocolTests(void)
{
    puts("Start protocol tests");

    // * UInt
    testSerializeUInt();
    testDeserializeUInt();

    // * UChar / Byte
    testSerializeUChar();
    testDeserializeUChar();

    // * String
    testSerializeUCharSet();
    testDeserializeUCharSet();

    // * Client Request Struct
    testSerializeClientRequest();
    testDeserializeClientRequest();

    // * Service Response Struct
    testSerializeServiceResponse();
    testDeserializeServiceResponse();
}

int main(int argc, char **argv)
{
    protocolTests();

    return 0;
}