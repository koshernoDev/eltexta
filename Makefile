all:	
	cd utils && $(MAKE) all 				\
	&& cd ../taxi_service && $(MAKE) all	\
	&& cd ../test_bench && $(MAKE) all		\
	&& cd

clean:
	cd taxi_service && $(MAKE) clean		\
	&& cd ../utils && $(MAKE) clean			\
	&& cd ../test_bench && $(MAKE) clean	\
	&& cd

# (09:43:27) egor.kogdin@jabber.eltex.loc: 
# for dir in $(DIRS) ; 
# do $(MAKEARCH) -j $(HOST_CPU_NUM) -C $$dir || exit 1 ; 
# done
# (09:43:39) egor.kogdin@jabber.eltex.loc: 
# DIRS    = lib user2 user 