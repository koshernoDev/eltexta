#ifndef UTILS_H
#define UTILS_H

#include <netinet/in.h>  // * sockaddr_in
#include <stdio.h>
#include <string.h>      // * memset()
#include <sys/socket.h>  // * socket(), bind(), listen(), accept(), close()
#include <sys/types.h>   // * sturct msghdr
#include <sys/wait.h>
#include <time.h>  // * time(), ctime()

#define _GNU_SOURCE
#include <fcntl.h>
#include <poll.h>
#include <unistd.h>  // * write(), sleep(), pipe()

#include "libancillary/ancillary.h"
#include "protocol.h"

#define LISTENERS 2U
#define MAX_DRIVERS 8U
#define MAX_CLIENTS 8U

#define COMMON_POLL_SIZE (LISTENERS + MAX_DRIVERS + MAX_CLIENTS)
#define CLIENT_POLL_START LISTENERS
#define CLIENT_POLL_END (CLIENT_POLL_START + MAX_CLIENTS)
#define DRIVER_POLL_START CLIENT_POLL_END
#define DRIVER_POLL_END (DRIVER_POLL_START + MAX_CLIENTS)

#define MAX_DRIVER_TASKS 8U

// * on failure: returns -1
int driverProcess(const unsigned char *buffer, const int ipcPipe[],
                  const int driverFD);

void printBuffer(const unsigned char *buffer);

// * on success: returns new listenerFD of server's socket for given ip and port
// * on failure: returns -1
int newIP4TCPServerAndListen(const char *ip, const char *port,
                             const char *maxConnections);

// * on failure: returns -1
int sendResponse(const int clientFD, const unsigned code, const char *message);

void checkArgs(int args, char *argv[]);

void printPolling(const long unsigned i, const char *where);

// * address of connecting peer
typedef struct IPCStruct {
  struct sockaddr_storage servClientStorage;
  struct sockaddr_storage servDriverStorage;
  socklen_t clientAddrSize;
  socklen_t driverAddrSize;
} IPCStruct;
void printIPCStruct(const IPCStruct *ipc);
// * common poll with listeners, clients and drivers
typedef struct PollStruct {
  struct pollfd common[COMMON_POLL_SIZE];
  struct pollfd *clientPoll;
  struct pollfd *driverPoll;
  struct pollfd actualPoll[COMMON_POLL_SIZE];
  unsigned actualSize;
  unsigned currentDrivers;
  unsigned currentClients;
} PollStruct;
// * common variables
typedef struct BigFnStruct {
  IPCStruct ipc;
  PollStruct poll;
  int clientsListener;
  int driversListener;
  unsigned char commonBuffer[BUF_SIZE];
  int ret;
} BigFnStruct;

void initBFS(BigFnStruct *bfs, char *argv[]);

void setPollAt(struct pollfd *poll, const unsigned pos, const int fd,
               const short eventsMask, const short reventsMask);
void initPollDefault(struct pollfd *poll, const int clientsListener,
                     const int driversListener);

void rejectNewConnection(const int listenerFD,
                         struct sockaddr_storage *servStorage,
                         socklen_t *__restrict addrSize, struct pollfd *poll,
                         const unsigned pos);

void acceptNewConnection(const int listenerFD,
                         struct sockaddr_storage *servStorage,
                         socklen_t *__restrict addrSize, struct pollfd *poll,
                         const unsigned pos);

void handleNewConnection(const int listenerFD,
                         struct sockaddr_storage *servStorage,
                         socklen_t *__restrict AddrSize, struct pollfd *poll,
                         unsigned *clients, const char *type);

void handleClient(const int fd, const unsigned i, struct pollfd *clientPoll,
                  unsigned *currentClients, struct pollfd *driverPoll,
                  const unsigned currentDrivers, const unsigned m,
                  const unsigned n, int driverToServicePairs[m][n],
                  struct pollfd *driverToServicePolls);

void handleDriver(const int fd, const unsigned i,
                  const struct pollfd *clientPoll,
                  const unsigned currentClients,
                  struct pollfd *driverPoll,
                  unsigned *currentDrivers, const unsigned m,
                  const unsigned n, int driverToServicePairs[m][n],
                  struct pollfd *driverToServicePolls);

void swapHandledPollWithLastPoll(struct pollfd *poll, const unsigned pos,
                                 const unsigned clients);

void handleBadRequest(struct pollfd *poll, const unsigned pos,
                      unsigned *currentClients);

void handleRequests(struct pollfd *poll, const unsigned size,
                    unsigned *currentClients, unsigned *currentDrivers,
                    struct pollfd *clientPoll, struct pollfd *driverPoll);

void packCommonPoll(BigFnStruct *bfs);

void printPollAt(const struct pollfd *poll, const unsigned pos,
                 const char *name);
void printPoll(const struct pollfd *poll, const unsigned size,
               const char *name);

void printActualAndCommonPolls(const PollStruct *ps);

#endif  // !UTILS_H