#include "utils.h"

int main(int args, char* argv[]) {
  checkArgs(args, argv);

  BigFnStruct bfs;
  initBFS(&bfs, argv);

  for (long unsigned i = 0;; i++) {
    printPolling(i, "BEGIN");
    // if (getchar() == (int)'q') return 0;
    packCommonPoll(&bfs);
    printActualAndCommonPolls(&bfs.poll);
    printPolling(i, "POLLED");
    if ((bfs.ret = poll(bfs.poll.actualPoll, bfs.poll.actualSize, -1)) < 1)
      continue;
    fprintf(stdout, "$\nnew poll event, ret = %d\n", bfs.ret);
    printActualAndCommonPolls(&bfs.poll);
    if (bfs.poll.actualPoll[0].revents & LISTENERS_MASK) {
      handleNewConnection(bfs.clientsListener, &bfs.ipc.servClientStorage,
                          &bfs.ipc.clientAddrSize, bfs.poll.clientPoll,
                          &bfs.poll.currentClients, "client");
    } else if (bfs.poll.actualPoll[1].revents & LISTENERS_MASK) {
      handleNewConnection(bfs.driversListener, &bfs.ipc.servDriverStorage,
                          &bfs.ipc.driverAddrSize, bfs.poll.driverPoll,
                          &bfs.poll.currentDrivers, "driver");
    } else {
      handleRequests(bfs.poll.actualPoll, bfs.poll.actualSize,
                     &bfs.poll.currentClients, &bfs.poll.currentDrivers,
                     bfs.poll.clientPoll, bfs.poll.driverPoll);
    }
    printPolling(i, "END");
  }

  return 0;
}

/*   for (;;) {
    // * receive block
    if ((clientFD =
             accept(clientsListener, (struct sockaddr *)&servClientStorage,
                    &clientAddrSize)) == -1) {
      fprintf(stderr, "parent: accept() == -1\n");

      close(clientFD);
      continue;
    }
    fprintf(stdout, "\n|new incoming connection #%lu, clientFD = %d\n",
            ++connections, clientFD);

    memset(commonBuffer, 0, BUF_SIZE);
    recv(clientFD, commonBuffer, BUF_SIZE, 0);
    if (deserializeEndPoint(commonBuffer, &endPoint) == -1) {
      fprintf(stderr, "parent: deserializeEndPoint() == -1\n");
      sendResponse(clientFD, UNKNOWN_PROTOCOL, "Unknown Protocol");
      close(clientFD);
      continue;
    }

    // * endpoints block
    if (endPoint == DRIVER) {
      fprintf(stdout, "parent: /driver endpoint\n");
      if (currentDrivers >= MAX_DRIVERS) {
        fprintf(stdout, "parent: /driver currentDrivers >= %u\n",
  MAX_DRIVERS); if (sendResponse(clientFD, DRIVERS_LIMIT_EXCEED, "Drivers
  Limit Exceed") == -1) { fprintf(stderr, "parent: /driver sendResponse(%u) ==
  -1\n", DRIVERS_LIMIT_EXCEED);
        }
        close(clientFD);
        continue;
      }
      // if (pipe(driverToServicePipes[currentDrivers]) == -1)
      // {
      //     fprintf(stderr, "parent: /driver pipe() == -1\n");
      //     if (sendResponse(clientFD, 500, "Internal Server Error") == -1)
      //     {
      //         fprintf(stderr, "parent: /driver sendResponse(500) == -1\n");
      //     }
      //     close(clientFD);
      //     continue;
      // }
      if (socketpair(AF_UNIX, SOCK_DGRAM, 0,
                     driverToServicePipes[currentDrivers]) == -1) {
        fprintf(stderr, "parent: /driver socketpair() == -1\n");
        if (sendResponse(clientFD, INTERNAL_SERVER_ERROR,
                         "Internal Server Error") == -1) {
          fprintf(stderr, "parent: /driver sendResponse(%u) == -1\n",
                  INTERNAL_SERVER_ERROR);
        }
        close(clientFD);
        continue;
      }
      fprintf(stdout,
              "parent: /driver driverToServicePipes[%u][0] = %d, "
              "driverToServicePipes[%u][1] = %d\n",
              currentDrivers, driverToServicePipes[currentDrivers][0],
              currentDrivers, driverToServicePipes[currentDrivers][1]);

      // * provide new driver process with read end of pipe
      // driverToServicePollFDs[currentDrivers].fd =
      // driverToServicePipes[currentDrivers][1];
      // driverToServicePollFDs[currentDrivers].events = POLLOUT;
      // * provide new driver process with read end of pair
      driverToServicePollFDs[currentDrivers].fd =
          driverToServicePipes[currentDrivers][1];
      driverToServicePollFDs[currentDrivers].events = POLLOUT;

      // * forking driver process
      pid_t connectionHandlePID = -1;
      if ((connectionHandlePID = fork()) == 0) {
        fprintf(stdout, "child: /driver forked driver child\n");
        if (driverProcess(commonBuffer, driverToServicePipes[currentDrivers],
                          clientFD) == -1) {
          fprintf(stderr, "child: childProcess == -1\n");
          sendResponse(clientFD, 434, "Driver Process Error");
          exit(EXIT_FAILURE);
        }
        sendResponse(clientFD, DRIVER_PROCESS_FINISHED,
                     "Driver Process Finished");
        close(clientFD);
        exit(EXIT_SUCCESS);
      } else if (connectionHandlePID > 0) {
        ++currentDrivers;
        fprintf(
            stdout,
            "parent: /driver forked new dirver process, currentDrivers =
  %u\n", currentDrivers);
        // close(clientFD);
      } else {
        fprintf(stderr, "parent: /driver fork() == -1\n");
        close(clientFD);
      }
    } else if (endPoint == CLIENT) {
      // ? have to fork?
      // * sending message to available driver process
      fprintf(stdout, "parent: /client: currentDrivers = %u\n",
  currentDrivers);
      // printBuffer(commonBuffer);
      if (currentDrivers == 0) {
        fprintf(stdout, "parent: /client ");
        sendResponse(clientFD, NO_AVAILABLE_DRIVERS, "No Available Drivers");
        close(clientFD);
        continue;
      }

      if (poll(driverToServicePollFDs, currentDrivers, -1) == -1) {
        fprintf(stderr, "parent: /client poll() == -1\n");
        sendResponse(clientFD, INTERNAL_SERVER_ERROR, "Internal Server
  Error"); close(clientFD); continue;
      }
      for (int i = 0; i < MAX_DRIVERS; ++i) {
        if (driverToServicePollFDs[i].revents & POLLOUT) {
          driverFD = driverToServicePollFDs[i].fd;
          fprintf(stdout,
                  "parent: /client: WRITING clientFD = %d on driverFD = %d\n",
                  clientFD, driverFD);

          if (sendMessageAndFDReference(clientFD, driverFD, commonBuffer) ==
              -1) {
            fprintf(stderr,
                    "parent: /client: sendMessageAndFDReference() == -1\n");
            sendResponse(clientFD, INTERNAL_SERVER_ERROR,
                         "Internal Server Error");
            close(clientFD);
            break;
          }

          // sendResponse(clientFD, CLIENT_GOT_DIRVER, "Client Got Driver");
          // close(clientFD);
          break;
        }
      }
    } else {
      fprintf(stdout, "parent: Unknown endpoint: %x\n", endPoint);
      if (sendResponse(clientFD, UNKNOWN_ENDPOINT, "Unknown Endpoint") == -1)
  { fprintf(stderr, "parent: sendResponse(%u) == -1\n", UNKNOWN_ENDPOINT);
      }
      close(clientFD);
      continue;  // * accept loop
    }
    // close(clientFD);
    // sleep(1); // * seconds
  }

  return 0; */

// // * address of connecting peer
// struct sockaddr_storage servClientStorage;
// struct sockaddr_storage servDriverStorage;
// socklen_t clientAddrSize = sizeof(servClientStorage);
// socklen_t driverAddrSize = sizeof(servDriverStorage);
// // * pipes and fds of current connections
// // ? remove
// //   int driverToServicePipes[MAX_DRIVERS][2];  // * 0 - read/out, 1 -
// //   write/in
// // 	 int clientToServicePipes[MAX_CLIENTS][2];  // * 0 - read/out, 1 -
// // write/in
// // * common poll with listeners, clients and drivers
// struct pollfd commonPoll[COMMON_POLL_SIZE];
// initPollDefault(commonPoll, clientsListener, driversListener);
// // printPoll(commonPoll, COMMON_POLL_SIZE);
// struct pollfd* clientPoll = commonPoll + CLIENT_POLL_START;
// struct pollfd* driverPoll = commonPoll + DRIVER_POLL_START;
// struct pollfd actualPoll[COMMON_POLL_SIZE];
// unsigned acutalSize = LISTENERS;
// // * common variables
// unsigned char commonBuffer[BUF_SIZE];
// memset(commonBuffer, 0, BUF_SIZE);
// unsigned currentDrivers = 0;
// unsigned currentClients = 0;
// int ret = -1;
//   unsigned endPoint = 0;
//   int clientFD = -1;
//   int driverFD = -1;
//   long unsigned int connections = 0;
