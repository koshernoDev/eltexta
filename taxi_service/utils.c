#include "utils.h"

int driverProcess(const unsigned char *buffer, const int socketPair[],
                  const int driverFD) {
  // * close write fd
  close(socketPair[1]);
  char procid[BUF_SIZE / 2];
  snprintf(procid, BUF_SIZE / 2, "driverProcess[%d]", driverFD);

  // * deserializing drivers data block
  TaxiDriverRequest tdReq;
  deserializeAndConstructDriverRequest(buffer, &tdReq);
  printTaxiDriverRequest(&tdReq);

  srandom((int)time(NULL));
  int tasks = 1 + (rand() % MAX_DRIVER_TASKS);
  fprintf(stdout, "^\tchild /driver %s: read on fd = %d\n", procid,
          socketPair[0]);
  fprintf(stdout, "^\tchild /driver %s: tasks = %d\n", procid, tasks);

  for (int i = 0; i < tasks; ++i) {
    unsigned char ipcBuffer[BUF_SIZE];
    memset(ipcBuffer, 0, BUF_SIZE);

    recv(socketPair[0], ipcBuffer, BUF_SIZE, 0);

    printBuffer(ipcBuffer);
    printProtocolBuffer(ipcBuffer);

    TaxiClientRequest tcReq;
    if (deserializeAndConstructClientRequest(ipcBuffer, &tcReq) == -1) {
      fprintf(
          stderr,
          "^\tchild /driver %s: deserializeAndConstructClientRequest() == -1\n",
          procid);

      continue;
    }

    fprintf(stdout, "^\tchild /driver %s: handling client: ", procid);
    printTaxiClientRequest(&tcReq);
    sleep(tcReq.time);

    // fprintf(stdout, "child /driver %s: res clientFD = %d, ", procid,
    // tcd->fd);
    // sendResponse(clientFDRef, 210, "Driver Served Client");
    // close(clientFDRef);
  }

  fprintf(stdout, "child /dirver %s: driver process finished\n", procid);

  return 0;
}

int newIP4TCPServerAndListen(const char *ip, const char *port,
                             const char *maxConnections) {
  if (port == NULL) {
    fprintf(stderr, "newIP4TCPServerAndListen: port == NULL\n");

    return -1;
  }

  int listenerFD = -1;

  if ((listenerFD = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    fprintf(stderr, "newIP4TCPServerAndListen: socket() == -1\n");

    return -1;
  }

  if (setsockopt(listenerFD, SOL_SOCKET, SO_REUSEADDR, &(int){1},
                 sizeof(int)) == -1) {
    fprintf(stderr, "newIP4TCPServerAndListen: setsockopt() == -1\n");

    return -1;
  }

  struct sockaddr_in addr;
  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(atoi((char *)port));
  addr.sin_addr.s_addr = inet_addr(ip == NULL ? "127.0.0.1" : ip);
  memset(addr.sin_zero, 0, sizeof addr.sin_zero);

  if (bind(listenerFD, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
    fprintf(stderr, "newIP4TCPServerAndListen: bind() == -1\n");

    return -1;
  }

  if (listen(listenerFD, atoi(maxConnections)) == -1) {
    fprintf(stderr, "newIP4TCPServerAndListen: listen() == -1\n");

    return -1;
  }

  fprintf(stdout, "listening on port: %u, listenerFD = %d\n",
          ntohs(addr.sin_port), listenerFD);

  return listenerFD;
}

int sendResponse(const int clientFD, const unsigned code, const char *message) {
  TaxiServiceResponse tsRes;
  constructNewTaxiServiceResponse(&tsRes, code, (unsigned char *)message);
  unsigned char buffer[BUF_SIZE];
  unsigned totalBytes = 0;
  serializeServiceResponse(buffer, &tsRes, &totalBytes);
  printTaxiServiceResponse(&tsRes);
  if (write(clientFD, buffer, totalBytes) == -1) {
    fprintf(stderr, "sendResponse() == -1\n");

    return -1;
  }

  destructTaxiServiceResponse(&tsRes);
  memset(buffer, 0, BUF_SIZE);

  return 0;
}

void checkArgs(int args, char *argv[]) {
  if (args != 4) {
    fprintf(stdout,
            "\nUsage: %s 1<client port> 2<driver port> 3<max connections>\n",
            argv[0]);
    exit(EXIT_FAILURE);
  }
}

void printPolling(const long unsigned i, const char *where) {
  fprintf(stdout, "\n==============POLLING #%lu %s==============\n", i, where);
}

void initBFS(BigFnStruct *bfs, char *argv[]) {
  memset(bfs, 0, sizeof(BigFnStruct));
  if ((bfs->clientsListener =
           newIP4TCPServerAndListen(NULL, argv[1], argv[3])) == -1) {
    fprintf(stderr, "main: newIP4TCPServerAndListen() == -1\n");
    exit(EXIT_FAILURE);
  }
  if ((bfs->driversListener =
           newIP4TCPServerAndListen(NULL, argv[2], argv[3])) == -1) {
    fprintf(stderr, "main: newIP4TCPServerAndListen() == -1\n");
    exit(EXIT_FAILURE);
  }
  bfs->ipc.clientAddrSize = sizeof(struct sockaddr_storage);
  bfs->ipc.driverAddrSize = sizeof(struct sockaddr_storage);

  bfs->poll.clientPoll = bfs->poll.common + CLIENT_POLL_START;
  bfs->poll.driverPoll = bfs->poll.common + DRIVER_POLL_START;
  bfs->poll.actualSize = LISTENERS;
  bfs->poll.currentClients = 0;
  bfs->poll.currentDrivers = 0;

  initPollDefault(bfs->poll.common, bfs->clientsListener, bfs->driversListener);

  bfs->ret = -1;
}

void printBuffer(const unsigned char *buffer) {
  int isFirst = 1;
  int i = BUF_SIZE;
  while (i--) {
    fprintf(stdout, "%s%s", isFirst ? "" : ".", buffer++);
    isFirst = 0;
  }

  puts("");
}

// * servise refactoring utils

void setPollAt(struct pollfd *poll, const unsigned pos, const int fd,
               const short eventsMask, const short reventsMask) {
  poll[pos].fd = fd;
  poll[pos].events = eventsMask;
  poll[pos].revents = reventsMask;
}

void initPollDefault(struct pollfd *poll, const int clientsListener,
                     const int driversListener) {
  setPollAt(poll, 0, clientsListener, LISTENERS_MASK, 0);
  setPollAt(poll, 1, driversListener, LISTENERS_MASK, 0);
  for (int i = LISTENERS; i < COMMON_POLL_SIZE; i++) {
    setPollAt(poll, i, -1, 0, 0);  // * available poll
  }
}

void rejectNewConnection(const int listenerFD,
                         struct sockaddr_storage *servStorage,
                         socklen_t *__restrict addrSize, struct pollfd *poll,
                         const unsigned pos) {
  int fd = accept(listenerFD, (struct sockaddr *)servStorage, addrSize);
  sendResponse(fd, LIMIT_EXCEED, "Limit Exceed");
  shutdown(fd, SHUT_RDWR);
  close(fd);
}

void acceptNewConnection(const int listenerFD,
                         struct sockaddr_storage *servStorage,
                         socklen_t *__restrict addrSize, struct pollfd *poll,
                         const unsigned pos) {
  int fd = accept(listenerFD, (struct sockaddr *)servStorage, addrSize);
  setPollAt(poll, pos, fd, CLIENTS_MASK, CLIENTS_MASK);
  printPollAt(poll, pos, "new client");
  sendResponse(fd, ACCEPTED, "Accepted");
}

void handleNewConnection(const int listenerFD,
                         struct sockaddr_storage *servStorage,
                         socklen_t *__restrict AddrSize, struct pollfd *poll,
                         unsigned *clients, const char *type) {
  if (*clients >= MAX_CLIENTS) {
    fprintf(stdout, "rejecting new %s\n", type);
    rejectNewConnection(listenerFD, servStorage, AddrSize, poll, *clients);
  } else {
    fprintf(stdout, "accepting new %s\n", type);
    acceptNewConnection(listenerFD, servStorage, AddrSize, poll, *clients);
    *clients = *clients + 1;
  }
}

void handleClient(const int fd, const unsigned i, struct pollfd *clientPoll,
                  unsigned *currentClients, struct pollfd *driverPoll,
                  const unsigned currentDrivers, const unsigned m,
                  const unsigned n, int driverToServicePairs[m][n],
                  struct pollfd *driverToServicePolls) {
  fprintf(stdout, "Handling client\n");
  if (currentDrivers == 0) {
    sendResponse(fd, NO_AVAILABLE_DRIVERS, "No Available Drivers");
  } else {
    sendResponse(fd, CLIENT_HANDLED, "Client Handled");
  }
  swapHandledPollWithLastPoll(clientPoll, i, *currentClients);
  *currentClients = *currentClients - 1;
  close(clientPoll[i].fd);
  // int driverFD = -1;
  // unsigned char buffer[BUF_SIZE];
  // memset(buffer, 0, BUF_SIZE);
  // for (int i = 0; i < drivers; ++i) {
  //   if (driverPoll[i].revents & POLLOUT) {
  //     driverFD = driverPoll[i].fd;
  //     fprintf(stdout,
  //             "parent: /client: WRITING clientFD = %d on driverFD = %d\n",
  //             fd, driverFD);

  //     if (sendMessageAndFDReference(fd, driverFD, buffer) == -1) {
  //       fprintf(stderr, "parent: /client: sendMessageAndFDReference() ==
  //       -1\n"); sendResponse(fd, INTERNAL_SERVER_ERROR, "Internal Server
  //       Error"); close(fd); break;
  //     }

  //     break;
  //   }
  // }
}

void handleDriver(const int fd, const unsigned i,
                  const struct pollfd *clientPoll,
                  const unsigned currentClients,
                  struct pollfd *driverPoll,
                  unsigned *currentDrivers, const unsigned m,
                  const unsigned n, int driverToServicePairs[m][n],
                  struct pollfd *driverToServicePolls) {
  fprintf(stdout, "Handling driver\n");
  sendResponse(fd, DRIVER_HANDLED, "Driver Handled");
  swapHandledPollWithLastPoll(driverPoll, i, *currentDrivers);
  *currentDrivers = *currentDrivers - 1;
  close(driverPoll[i].fd);

  // socketpair(AF_UNIX, SOCK_DGRAM, 0, driverToServicePairs[currentDrivers]);

  // fprintf(stdout,
  //         "parent: /driver driverToServicePairs[%u][0] = %d, "
  //         "driverToServicePairs[%u][1] = %d\n",
  //         currentDrivers, driverToServicePairs[currentDrivers][0],
  //         currentDrivers, driverToServicePairs[currentDrivers][1]);

  // // * provide new driver process with read end of pair
  // driverToServicePolls[currentDrivers].fd =
  //     driverToServicePairs[currentDrivers][1];
  // driverToServicePolls[currentDrivers].events = POLLOUT;

  // // * forking driver process
  // const unsigned char commonBuffer[BUF_SIZE];
  // return;
  // pid_t driverPID = -1;
  // if ((driverPID = fork()) == 0) {
  //   fprintf(stdout, "^\tchild: /driver forked driver child\n");
  //   driverProcess(commonBuffer, driverToServicePairs[currentDrivers], fd);
  //   sendResponse(fd, DRIVER_PROCESS_FINISHED, "Driver Process Finished");
  //   close(fd);
  //   exit(EXIT_SUCCESS);
  // } else if (driverPID > 0) {
  //   // sendResponse(fd, DRIVER_HANDLED, "Driver Handled");
  //   // int status;
  //   // waitpid(driverPID, &status, 0);
  //   close(driverPoll[i].fd);
  // } else {
  //   fprintf(stderr, "parent: /driver fork() == -1\n");
  //   sendResponse(fd, INTERNAL_SERVER_ERROR, "Internal Server Error");
  //   close(fd);
  // }
}

void handleRequests(struct pollfd *poll, const unsigned size,
                    unsigned *currentClients, unsigned *currentDrivers,
                    struct pollfd *clientPoll, struct pollfd *driverPoll) {
  fprintf(stdout, "\n!\nhandleRequests:\n");
  fprintf(stdout, "clients = %u, drivers = %u\n", *currentClients,
          *currentDrivers);

  const unsigned pairs = MAX_DRIVERS;
  const unsigned pair = 2;
  int driverToServicePairs[pairs][pair];
  struct pollfd driverToServicePolls[pairs];

  for (int i = 0; i < *currentClients; i++) {
    printPollAt(clientPoll, i, "pre handle clients");

    switch (clientPoll[i].revents) {
      case CLIENTS_MASK:
        fprintf(stdout, "clients mask\n");
        handleClient(clientPoll[i].fd, i, clientPoll, currentClients,
                     driverPoll, *currentDrivers, pairs, pair,
                     driverToServicePairs, driverToServicePolls);
        break;
      case 0:
        fprintf(stderr, "no revent\n");
        break;
      default:
        handleBadRequest(clientPoll, i, currentClients);
    }
    printPollAt(clientPoll, i, "post handle clients");
  }

  for (int i = 0; i < *currentDrivers; i++) {
    printPollAt(driverPoll, i, "pre handle drivers");

    switch (driverPoll[i].revents) {
      case CLIENTS_MASK:
        fprintf(stdout, "clients mask\n");
        handleDriver(driverPoll[i].fd, i, clientPoll, *currentClients,
                     driverPoll, currentDrivers, pairs, pair,
                     driverToServicePairs, driverToServicePolls);
        break;
      case 0:
        fprintf(stderr, "no revent\n");
        break;
      default:
        handleBadRequest(driverPoll, i, currentDrivers);
    }
    printPollAt(driverPoll, i, "post handle drivers");
  }
  puts("!\n");
}

void swapHandledPollWithLastPoll(struct pollfd *poll, const unsigned pos,
                                 const unsigned size) {
  if (size == 0) {
    return;
  }
  if (size == 1) {
    setPollAt(poll, 0, -1, 0, 0);
    return;
  }
  const unsigned lastInd = size - 1;
  setPollAt(poll, pos, poll[lastInd].fd, poll[lastInd].events,
            poll[lastInd].revents);
  setPollAt(poll, lastInd, -1, 0, 0);
}

void handleBadRequest(struct pollfd *poll, const unsigned pos,
                      unsigned *currentClients) {
  fprintf(stderr, "unexpected revent\n");
  swapHandledPollWithLastPoll(poll, pos, *currentClients);
  *currentClients = *currentClients - 1;
  sendResponse(poll[pos].fd, BAD_REQUEST, "Bad Request");
  close(poll[pos].fd);
}

void packCommonPoll(BigFnStruct *bfs) {
  int j = 0;
  fprintf(stdout, "@\npackCommonPoll: \n");
  for (size_t i = 0; i < LISTENERS; i++, j++) {
    setPollAt(bfs->poll.actualPoll, j, bfs->poll.common[i].fd,
              bfs->poll.common[i].events, bfs->poll.common[i].revents);
    // printPollAt(bfs->poll.common, i, "common listen");
    // printPollAt(bfs->poll.actualPoll, j, "actual listen");
  }
  for (size_t i = 0; i < bfs->poll.currentClients; i++, j++) {
    setPollAt(bfs->poll.actualPoll, j, bfs->poll.clientPoll[i].fd,
              bfs->poll.clientPoll[i].events, bfs->poll.clientPoll[i].revents);
    // printPollAt(bfs->poll.common, CLIENT_POLL_START + i, "common clients");
    // printPollAt(bfs->poll.actualPoll, j, "actual clients");
  }
  for (size_t i = 0; i < bfs->poll.currentDrivers; i++, j++) {
    setPollAt(bfs->poll.actualPoll, j, bfs->poll.driverPoll[i].fd,
              bfs->poll.driverPoll[i].events, bfs->poll.driverPoll[i].revents);
    // printPollAt(bfs->poll.common, DRIVER_POLL_START + i, "common drivers");
    // printPollAt(bfs->poll.actualPoll, j, "actual drivers");
  }
  fprintf(stdout, "@\n");
  bfs->poll.actualSize = j;
}

void printPollAt(const struct pollfd *poll, const unsigned pos,
                 const char *name) {
  fprintf(stdout, "%s poll[%u] = { fd = %d, events = 0x%x, revents = 0x%x }\n",
          name, pos, poll[pos].fd, poll[pos].events, poll[pos].revents);
}

void printPoll(const struct pollfd *poll, const unsigned size,
               const char *name) {
  fprintf(stdout, "%s poll size = %u\n", name, size);
  for (size_t i = 0; i < size; i++) {
    printPollAt(poll, i, name);
  }
}

void printActualAndCommonPolls(const PollStruct *ps) {
  fprintf(stdout, "\n#\n");
  printPoll(ps->common, COMMON_POLL_SIZE, "common");
  printPoll(ps->actualPoll, ps->actualSize, "actual");
  printPoll(ps->clientPoll, ps->currentClients, "client");
  printPoll(ps->driverPoll, ps->currentDrivers, "driver");
  fprintf(stdout, "#\n");
}